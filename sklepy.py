import ftplib
import glob
import os
import socket
import zipfile
import datetime
import smtplib
import db_connect as db
import shutil

ftp_user = 'Tommy'
ftp_password='prod98'


gmail_user = 'marek.milcarz@nielsen.com'
gmail_password = 'knkouhzxasxywufh'


def dniMiesiace(past=0):
    dayz = 7
    now=datetime.datetime.now()-datetime.timedelta(days= past)
    month, day = now.month, now.day
    week = (datetime.datetime.now() - datetime.timedelta(days=dayz)).isocalendar()[1]
    year = (datetime.datetime.now()-datetime.timedelta(days=dayz+past)).year
    if len(str(day)) == 1:
        day = '0' + str(day)

    if len(str(month)) == 1:
        month = '0' + str(month)

    if len(str(week)) == 1:
        week = '0' + str(week)

    return {'year':str(year),'month':str(month),'day':str(day), 'week':str(week)}

def mail(sklep):
    sent_from = 'marek.milcarz@nielsen.com'
    to = ['katarzyna.grochowska@nielsen.com', 'marek.milcarz@nielsen.com', 'malgorzata.mergner@nielsen.com',
          'wojciech.oldakowski@nielsen.com']
    subject = 'Sirpair production!'
    body = '''
Czesc,

\(^ _ ^)/    Zrobilem {sklep} o: {czas}     \(^ _ ^)/

Skryptor'''.format(sklep=sklep, czas = str(datetime.datetime.now()))

    email_text = 'From: {}\nTo: {}\nSubject: {}\n\n{}'.format(sent_from, to, subject, body)

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    server.sendmail(sent_from, to, email_text)
    server.close()


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! UPDATE THIS AFTER NEW YEAR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#TODO
def period_update(period_change=0):

    con = db.engine.connect()#nawiazuje polaczenie z sqlem

    SQL_statement = '''select max( nc_periodid) from VLDINSTR_NO.PERIODS where  nc_periodopen = 1'''#kwerenda co chc uzyskac (bez ; na koncu)

    outpt = con.execute(SQL_statement)#zrob polecenie z sqla
    df = outpt.fetchall()#zapisz do zmiennej output
    period = df[0][0]
    con.close()#zamykam polaczenie

    if period_change != 0:
        period = period_change

    period = int(period)

    return period

#MX
def MX(past=0, period_change=0):

    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/MIX')

        past=past+0 # bazowo 0
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\Mix')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test')

        filename = "MIX_" + str(year)+str(week) + "_" + year + month + day + "*"
        print(filename)
        listOfFiles=[]
        for file in ftp.nlst(filename):
            listOfFiles.append(file)

        if len(listOfFiles) != 0:
            print("Getting: "+listOfFiles[0])
            fhandle = open(listOfFiles[0], 'wb')
            ftp.retrbinary('RETR ' + listOfFiles[0],fhandle.write)
            fhandle.close()

            shutil.copy(listOfFiles[0], r'M:\NO\NO_MIX\\')

            os.rename(listOfFiles[0],"oms"+str(year[-2:])+str(week)+"0.000")

            try:
                mail('MX')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem MX (/^▽^)/')
        else:
             return print('Nie znalazlem pliku (ಠ ∩ಠ)')
    except socket.timeout:
        MX()
    try:
        ftp.quit()
    except:
        pass
#HY
def HY(past = 0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/unox')

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\HYDROTEX')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test')
        past=past+1 # bazowo -1
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        filename = "ACN_"+year+month+day+ str(week)+".txt"

        listOfFiles=[]
        for file in ftp.nlst(filename):
            listOfFiles.append(file)

        if len(listOfFiles) != 0:
            print("Getting: "+filename)
            fhandle = open(filename, 'wb')
            ftp.retrbinary('RETR ' + filename,fhandle.write)
            fhandle.close()

            shutil.copy(filename, r'M:\NO\NO_YX\\')

            os.rename(filename,"oms"+str(year[-2:])+str(week)+"0.000")


            try:
                mail('HY')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem HY (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')
    except socket.timeout:
        HY()
    try:
        ftp.quit()
    except:
        pass
#daily CO
def dailyCO(past=0, period_change=0):

    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/coop')

        dateDct = dniMiesiace()
        year, week = dateDct['year'], dateDct['week']

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\COOP\W' + str(year[-2:])+str(week))


        dateDct = dniMiesiace(past)
        year, month, day = dateDct['year'], dateDct['month'], dateDct['day']

        tryList = []
        try:
            for file in glob.glob('coop2acn_' + year + month + day + '0215'):
                os.remove(file)
        except:
            pass

        filename = 'coop2acn_' + year + month + day + '*'
        filename2 = 'zextbpos_' + year + month + day + '_*.csv'
        for file in ftp.nlst(filename):
            tryList.append(file)
        if len(tryList) != 0:
                for file in ftp.nlst(filename):
                    print("Getting: " + file)
                    fhandle = open(file, 'wb')
                    ftp.retrbinary('RETR ' + file, fhandle.write)
                    fhandle.close()

                for file in ftp.nlst(filename2):
                    print("Getting: " + file)
                    fhandle = open(file, 'wb')
                    ftp.retrbinary('RETR ' + file, fhandle.write)
                    fhandle.close()
                    try:
                        mail('CO'+str(day))
                    except:
                        print('Nie udało się wysłać maila')
                        raise
                    return print('Zrobiłem CO (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')
    except socket.timeout:
        dailyCO()
    try:
        ftp.quit()
    except:
        pass
# monday CO
def CO(past = 0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/coop')

        i,d=0,0
        listOfFiles = []
        listOfCSV =[]
        tryList=[]




        past=past+0#bazowo 0
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']
        prime = dateDct['day']
        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\COOP\W' + str(year[-2:])+str(week))
        filename, filename2 = 'coop2acn_' + year + month + day + '*', 'zextbpos_' + year + month + day + '_*.csv'

        for file in ftp.nlst(filename):
            tryList.append(file)


        if len(tryList) != 0:
            try:
                while d < 4:
                    dateDct = dniMiesiace(d+past)
                    year, month, day = dateDct['year'], dateDct['month'], dateDct['day']
                    for file in glob.glob('coop2acn_' + year + month + day + '0215'):
                        os.remove(file)
                    d+=1
            except:
                pass
            d=0
            while d < 4:
                #bazowo 0
                dateDct = dniMiesiace(d+past)
                year, month, day = dateDct['year'], dateDct['month'], dateDct['day']
                filename, filename2 = 'coop2acn_' + year + month + day + '*', 'zextbpos_' + year + month + day + '_*.csv'

                for file in ftp.nlst(filename):
                    print("Getting: " + file)
                    fhandle = open(file, 'wb')
                    ftp.retrbinary('RETR ' + file, fhandle.write)
                    fhandle.close()

                for file in ftp.nlst(filename2):
                    print("Getting: " + file)
                    fhandle = open(file, 'wb')
                    ftp.retrbinary('RETR ' + file, fhandle.write)
                d+=1

            while i < 7:
                dateDct = dniMiesiace(i+past)
                year = dateDct['year']
                month = dateDct['month']
                day = dateDct['day']
                filename = 'coop2acn_' + year + month + day + '*'
                filename2 = 'zextbpos_' + year + month + day + '_*.csv'

                for file in ftp.nlst(filename):
                    listOfFiles.append(file)

                for fileCSV in ftp.nlst(filename2):
                    listOfCSV.append(fileCSV)

                i+=1


            with open('add_ncoop.bat', 'w') as add:
                add.writelines(map('rawcoopn {} {}\n'.format, listOfCSV, listOfFiles))

            try:
                mail('CO '+str(prime))
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem CO (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')
    except socket.timeout:
        CO()
    try:
        ftp.quit()
    except:
        pass

#thursday CO
def midweekCO(past=0, period_change = 0):
    try:
        # ftp.login('Tommy', password)
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/coop')

        j,d = 1,0
        tryList=[]

        past=past+0 #bazowo 0
        dateDct = dniMiesiace(past)
        year,month,day, week = dateDct['year'],dateDct['month'],dateDct['day'], dateDct['week']
        filename, filename2= 'coop2acn_' + year + month + day + '*', 'zextbpos_' + year + month + day + '_*.csv'
        week = str(int(week)+1)
        if len(week)==1:
            week = '0'+week
        np = r'\\Acn047oslfsv04\s\RAWINPUT\COOP\W' + str(year[-2:]) + str(week)
        if not os.path.exists(np):
            os.makedirs(np)
        os.chdir(np)

        for file in ftp.nlst(filename):
            tryList.append(file)
        if len(tryList) != 0:
            while d<3:
                dateDct = dniMiesiace(past+d)
                year, month, day = dateDct['year'], dateDct['month'], dateDct['day']
                filename, filename2 = 'coop2acn_' + year + month + day + '*', 'zextbpos_' + year + month + day + '_*.csv'

                for file in ftp.nlst(filename):
                    fhandle = open(file, 'wb')
                    ftp.retrbinary('RETR ' + file, fhandle.write)
                    fhandle.close()

                for file in ftp.nlst(filename2):
                    fhandle = open(file, 'wb')
                    ftp.retrbinary('RETR ' + file, fhandle.write)
                    fhandle.close()
                d+=1

            while j <5:
                dateDct = dniMiesiace(past-j)
                year = dateDct['year']
                month = dateDct['month']
                day = dateDct['day']
                open('coop2acn_' + year + month + day+'0215', 'a').close()
                j+=1
            try:
                mail('CO'+str(day))
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem CO (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        midweekCO()
    try:
        ftp.quit()
    except:
        pass

#LY
def LY(period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/bunnpris/Bunnpris')

        i, j = 0, 0

        dateDct = dniMiesiace()
        year, week = dateDct['year'], dateDct['week']

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\LYKBUNHK')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test')

        filename = "oms"+str(year[-2:])+str(week)+"*.*"

        listOfFiles=[]
        for file in ftp.nlst(filename):
            if j<1:
                listOfFiles.append(file)
                j+=1
        if len(listOfFiles) != 0:
            for file in ftp.nlst(filename):
                i = i + 1
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()

                shutil.copy(file, r'M:\NO\NO_BUNNPRIS\\')
            print('Pobrałem: ' + str(i) + ' plików.')
            try:
                mail('LY')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem LY (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        LY()
    try:
        ftp.quit()
    except:
        pass
#REMA
def RE(past=0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/vbd/rema')

        past=past+2 #bazowo 2
        dateDct = dniMiesiace(past)
        month, day, year, week = dateDct['month'], dateDct['day'],dateDct['year'], dateDct['week']

        filename = "*W"+str(week)+"*.zip"


        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\REMA\zipmail')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test\Zipmail')

        listOfFiles=[]
        for file in ftp.nlst(filename):
            listOfFiles.append(file)


        if len(listOfFiles) != 0:
            for file in ftp.nlst(filename):
                print("Getting: "+file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()

                shutil.copy(file, r'M:\NO\NO_REMA\\')

                with zipfile.ZipFile(r'\\Acn047oslfsv04\s\RAWINPUT\REMA\zipmail\\'+str(file),"r") as zip_ref:
                    zip_ref.extractall(r'\\Acn047oslfsv04\s\RAWINPUT\REMA\\')
                    os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\REMA\\')
                # with zipfile.ZipFile(r'C:\Users\mima8005\Desktop\Test\zipmail\\'+str(file),"r") as zip_ref:
                #     zip_ref.extractall(r'C:\Users\mima8005\Desktop\Test')
                #     os.chdir(r'C:\Users\mima8005\Desktop\Test')
                    for file in glob.glob("OMS"+month+day+"*.*"):

                        os.rename(file,"oms"+str(year[-2:])+str(week)+file[-5:])
            try:
                mail('RE')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem RE (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        RE()
    try:
        ftp.quit()
    except:
        pass

#VB
def VB(past=0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/norgesgruppen/spar')

        past=past+0#bazowo 0
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        filename = "RIGAL*" + year + month + day + "*.zip"

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\NBHK\zipmail')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test\Zipmail')

        listOfFiles=[]
        for file in ftp.nlst(filename):
            listOfFiles.append(file)

        if len(listOfFiles) != 0:
            for file in ftp.nlst(filename):
                print("Getting: "+file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()

                shutil.copy(file, r'M:\NO\NO_SparJoker\\')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

        past=past+2 #bazowo 2
        dateDct = dniMiesiace(past)
        month = dateDct['month']
        day = dateDct['day']


        print('Wypakowuję. Chwilę to potrwa... ¯\_( ◉ 3 ◉ )_/¯')

        for file in ftp.nlst(filename):
            with zipfile.ZipFile(r'\\Acn047oslfsv04\s\RAWINPUT\NBHK\zipmail\\'+str(file),"r") as zip_ref:
                zip_ref.extractall(r'\\Acn047oslfsv04\s\RAWINPUT\NBHK\\')
                os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\NBHK\\')
                for file in glob.glob("OMS"+month+day+"*.*"):
                    os.rename(file,"oms"+str(year[-2:])+str(week)+file[-5:])

        # for file in ftp.nlst(filename):
        #     with zipfile.ZipFile(r'C:\Users\mima8005\Desktop\Test\zipmail\\'+str(file),"r") as zip_ref:
        #         zip_ref.extractall(r'C:\Users\mima8005\Desktop\Test\\')
        #         os.chdir(r'C:\Users\mima8005\Desktop\Test\\')
        #         for file in glob.glob("OMS"+str(month)+str(day)+"*.*"):
        #             os.rename(file,"oms"+str(per2)+file[-5:])

        try:
            mail('VB')
        except:
            print('Nie udało się wysłać maila')
            raise
        return print('Zrobiłem VB (/^▽^)/')

    except socket.timeout:
        VB()
    try:
        ftp.quit()
    except:
        pass
#KI
def KI(past=0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/norgesgruppen/kiwi')

        past=past+0
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        filename = "RIGAL*"+year+month+day+"*.zip"

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\KIWIHK\zipmail')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test\Zipmail')

        listOfFiles = []
        for file in ftp.nlst(filename):
            listOfFiles.append(file)

        if len(listOfFiles) != 0:
            for file in ftp.nlst(filename):
                print("Getting: "+file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()

                shutil.copy(file, r'M:\NO\NO_KIWI\\')

                past=past+2#bazowo 2
                dateDct = dniMiesiace(past)
                month = dateDct['month']
                day = dateDct['day']

                # with zipfile.ZipFile(r'C:\Users\mima8005\Desktop\Test\zipmail\\'+str(file),"r") as zip_ref:
                #     zip_ref.extractall(r'C:\Users\mima8005\Desktop\Test\\')
                #     os.chdir(r'C:\Users\mima8005\Desktop\Test\\')
                #     for file in glob.glob("OMS"+str(month)+str(day)+"*.*"):
                #         os.rename(file,"oms"+str(per2)+file[-5:])
                with zipfile.ZipFile(r'\\Acn047oslfsv04\s\RAWINPUT\KIWIHK\zipmail\\'+str(file),"r") as zip_ref:
                    zip_ref.extractall(r'\\Acn047oslfsv04\s\RAWINPUT\KIWIHK\\')
                    os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\KIWIHK\\')
                    for file in glob.glob("OMS"+month+day+"*.*"):
                        os.rename(file,"oms"+str(year[-2:])+str(week)+file[-5:])
            try:
                mail('KI')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem KI (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        KI()
    try:
        ftp.quit()
    except:
        pass

#ME
def ME(past=0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/norgesgruppen/meny')

        past=past+0
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        #dniimiesiace(day,month)

        filename = "RIGAL_"+year+month+day+"*.zip"

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\MENYHK\zipmail')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test\Zipmail')

        listOfFiles = []
        for file in ftp.nlst(filename):
            listOfFiles.append(file)

        if len(listOfFiles) != 0:
            print("Getting: "+file)
            fhandle = open(file, 'wb')
            ftp.retrbinary('RETR ' + file,fhandle.write)
            fhandle.close()

            shutil.copy(file, r'M:\NO\NO_Meny\\')

            past=past+2 #bazowo 2
            dateDct = dniMiesiace(past)
            month = dateDct['month']
            day = dateDct['day']

            with zipfile.ZipFile(r'\\Acn047oslfsv04\s\RAWINPUT\MENYHK\zipmail\\'+str(file),"r") as zip_ref:
                zip_ref.extractall(r'\\Acn047oslfsv04\s\RAWINPUT\MENYHK\\')
            os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\MENYHK\\')
            for file in glob.glob("OMS"+month+day+"*.*"):
                os.rename(file,"oms"+str(year[-2:])+str(week)+file[-5:])

            # with zipfile.ZipFile(r'C:\Users\mima8005\Desktop\Test\zipmail\\'+str(file),"r") as zip_ref:
            #     zip_ref.extractall(r'C:\Users\mima8005\Desktop\Test\\')
            #     os.chdir(r'C:\Users\mima8005\Desktop\Test\\')
            #     for file in glob.glob("OMS"+str(month)+str(day)+"*.*"):
            #         os.rename(file,"oms"+str(per2)+file[-5:])
            try:
                mail('ME')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem ME (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        ME()
    try:
        ftp.quit()
    except:
        pass

# NG
def NG(past=0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/norgesgruppen/cc-mat')

        past=past+0 #bazowo 0
        dateDct=dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        filename, oldFile = "RIGAL_" + str(year) + str(month) + str(day) + "*.zip", 'OMS'+str(year[-2:])+str(week)+'*.*'


        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\CCMARTN')

        for file in glob.glob(oldFile):
            os.remove(file)

        os. chdir(r'\\Acn047oslfsv04\s\RAWINPUT\CCMARTN\zipmail')

        listOfFiles = []
        for file in ftp.nlst(filename):
            listOfFiles.append(file)

        if len(listOfFiles) != 0:
            for file in ftp.nlst(filename):
                print("Getting: " + file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file, fhandle.write)
                fhandle.close()

                shutil.copy(file, r'M:\NO\NO_CCMartin\\')

                past=past+2 #bazowow 2
                dateDct = dniMiesiace(past)
                month = dateDct['month']
                day = dateDct['day']

                with zipfile.ZipFile(r'\\Acn047oslfsv04\s\RAWINPUT\CCMARTN\zipmail\\' + str(file), "r") as zip_ref:
                    zip_ref.extractall(r'\\Acn047oslfsv04\s\RAWINPUT\CCMARTN\\')

                os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\CCMARTN\\')
                for file in glob.glob("OMS" + str(month) + str(day) + "*.*"):
                    os.rename(file, "oms" + str(year[-2:])+str(week) + file[-5:])

                past=past-7#bazowo 7 (5 dni wstecz - bo wczesniej dodaje 2
                dateDct = dniMiesiace(past)
                month = dateDct['month']
                day = dateDct['day']
                week = dateDct['weel']

                for file in glob.glob("OMS" + str(month) + str(day) + "*.*"):
                    os.rename(file, "oms" + str(year[-2:])+str(week) + file[-5:])
            try:
                mail('NG')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem NG (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        NG()
    try:
        ftp.quit()
    except:
        pass
#RS
def RS(past=0, period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)
        ftp.cwd('/Reitan Servicehandel Mottak')

        past=past+0 #bazowo 0
        dateDct = dniMiesiace(past)
        year, month, day, week = dateDct['year'], dateDct['month'], dateDct['day'], dateDct['week']

        filename = "ACN"+year[2:]+str(month)+str(day)+"*.txt"

        os.chdir(r'\\Acn047oslfsv04\s\RAWINPUT\RSH')
        #os.chdir(r'C:\Users\mima8005\Desktop\Test')

        listOfFiles = []
        for file in ftp.nlst(filename):
            listOfFiles.append(file)
        if listOfFiles != 0:
            for file in listOfFiles:
                print("Getting: "+file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()

                shutil.copy(file, r'M:\NO\NO_RSH\\')


            os.rename(file,"oms"+str(year[-2:])+str(week)+"0.000")
            try:
                mail('RS')
            except:
                print('Nie udało się wysłać maila')
                raise
            return print('Zrobiłem RS (/^▽^)/')
        else:
            return print('Nie znalazlem pliku (ಠ ∩ಠ)')

    except socket.timeout:
        ftp.set_pasv(True)
        RS()
    try:
        ftp.quit()
    except:
        pass

