import db_connect as db
import pandas as pd
import datetime
from sklepy import period_update, dniMiesiace

con = db.engine.connect()

#automatyczny update periodu
period = period_update()
acv_tollerance = 20
receipent = 1800009060

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

SQL_statement = '''
select unique ac_nshopid from storemonitor
where nc_periodid= {period}
and ac_dtgroup= 'VOLUMETRIC'
and ac_monitortag= '999999' and ac_monitorstatus = 3
'''
SQL_statement = SQL_statement.format(period=period)
print(SQL_statement)
outpt2 = con.execute(SQL_statement)
df2 = pd.DataFrame(outpt2.fetchall())
df2.columns = outpt2.keys()
df2.ac_nshopid = df2.ac_nshopid.astype(float)




file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\MissingDisplay_{period}.xlsx'.format(
    period=str(period + 199818))
df2['ac_nshopid'] = df2['ac_nshopid'] - 1800000000
file = r'\\acn047oslfsv05\statavd\Sample\Sample.xlsx'
data = pd.ExcelFile(file)
print(data.sheet_names)
df1 = data.parse('Sample')
print(df1.info())
donors = pd.merge(
    df1[['MTNR', 'ActiveInSample', 'CEL_ID', 'ACV', 'NO_SHOPSIZE', 'NO_RETAILER', 'NO_KONSEPTKJEDE', 'NO_AREA','NO_FYLKE']], df2,
    left_on='MTNR', right_on='ac_nshopid', how='right')
donors.ac_nshopid = donors.ac_nshopid.astype(int)
donors.ac_nshopid = donors.ac_nshopid.astype(str)
donors = donors[donors['ActiveInSample'] == 'YES']
donors.ACV = donors.ACV.astype(int)
#str(receipent - 1800000000)
receipent_char = df1[df1['MTNR'] == receipent - 1800000000].reset_index()
print(receipent_char)
donors['ACV_text'] = donors.apply(lambda row: abs(row['ACV'] - receipent_char.at[0, 'ACV']) < acv_tollerance, axis=1)
filtr = (donors['CEL_ID'] == receipent_char.at[0, 'CEL_ID']) & (donors['NO_AREA'] == receipent_char.at[0, 'NO_AREA']) & (donors['NO_FYLKE'] == receipent_char.at[0, 'NO_FYLKE'])

donors = donors[filtr][donors['ACV_text']]

print(donors)




