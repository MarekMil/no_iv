import os
import zipfile
from sklepy import dniMiesiace
import ftplib
import gzip
import glob
import shutil


retailers = ('MIX', 'YX', 'RSH', 'BUNNPRIS', 'SHELL', 'CCMARTIN', 'MENY', 'KIWI', 'REMA', 'CIRCLEK', 'SPARJOKER')
butoms = ('BUNNPRISBUTOMS', 'CCMARTINBUTOMS', 'MENYBUTOMS', 'KIWIBUTOMS', 'REMABUTOMS', 'SPARJOKERBUTOMS')



filepath = r'\\Acn047oslfsv04\s\srvlload\\'
filepath_butoms = filepath+'butoms\\'

def seff_drop(past=0, period_change=0):
    try:
        ftp = ftplib.FTP('10.90.120.80','anonymous')

        ftp.cwd('/sircdbwork')

        dateDct = dniMiesiace(past)
        year, week = dateDct['year'], dateDct['week']
        year = '2019'
        week = '04'
        file_start ='PSCNO.SIR.PSCNO'
        file_end = '.WK'+str(year)+'0'+str(week)+'.ZIP.dat.gz'

        os.chdir(filepath)

        for ret in retailers:
            for file in ftp.nlst(file_start+ret+file_end):
                print("Getting: "+file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()
            try:
                with gzip.open(file_start+ret+file_end, 'rb') as s_file, open(file_start+ret+file_end[:-3], 'wb') as d_file:
                    shutil.copyfileobj(s_file, d_file)
                with zipfile.ZipFile(filepath+file_start+ret+file_end[:-3],"r") as zip_ref:
                    zip_ref.extractall()
            except:
                print('Nie wypakowalem {}'.format(ret))
                pass

        os.chdir(filepath_butoms)

        for but in retailers:
            for file in ftp.nlst(file_start+ret+file_end):
                print("Getting: "+file)
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file,fhandle.write)
                fhandle.close()

            try:
                with gzip.open(file_start+but+file_end, 'rb') as s_file, open(file_start+but+file_end[:-3], 'wb') as d_file:
                    shutil.copyfileobj(s_file, d_file)
                with zipfile.ZipFile(file_start+but+file_end[:-3],"r") as zip_ref:
                    zip_ref.extractall()
            except:
                pass

        ftp.close()

    except:
        print('Couldn\'t transfer files')
        raise

seff_drop()
