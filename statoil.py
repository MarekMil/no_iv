import pandas as pd
from sklepy import dniMiesiace


def vatST(period_change=0):
    dateDct = dniMiesiace(period_change * 7)
    year, week = dateDct['year'], dateDct['week']

    name_ra1 = r'\\Acn047oslfsv04\s\srvlload\ST' + str(year[-2:])+str(week) + '.RA1'
    # name_ra1 = r'C:\Users\mima8005\Desktop\NO IV\Python\Python_projects\ST1824.RA1'
    file = r'\\Acn047oslfsv04\s\srvlload\Statoil_PC.xlsx'

    vat = pd.read_excel(file, header=None, index_col=None, sheet_name='oms16390', dtype=str)
    # print(vat.head())

    ra1 = pd.read_csv(name_ra1, sep=';', dtype=str, encoding='ansi', header=None)
    ra1.columns = ['row']
    ra1[1] = ra1.apply(lambda row: row[0][69:76].strip(), axis=1)

    ra1['sales'] = ra1.apply(lambda row: row[0][41:55].strip(), axis=1)
    # ra1.sales = ra1.sales.astype(float)

    ra1vat = pd.merge(ra1, vat, how='left', left_on=[1], right_on=[0])
    print(ra1vat.head())

    def vat_rate(origin, sale, cat):
        if cat == 'H':
            return origin[0:58] + str(round(float(sale) / 1.25)) + ' ' * (
                        10 - len(str(int(float(sale) / 1.25)))) + origin[68:]
        elif cat == 'L':
            return origin[0:58] + str(round(float(sale) / 1.15)) + ' ' * (
                        10 - len(str(int(float(sale) / 1.15)))) + origin[68:]
        else:
            return origin

    ra1vat['test'] = ra1vat.apply(lambda row: vat_rate(row['row'], row['sales'], row[2]), axis=1)
    # ra1vat['test'] = ra1vat.apply(lambda row: vat_rate(row['sales'], row[2]), axis=1)
    print(ra1vat['test'].head())
    ra1vat.test = ra1vat.test.astype(str)
    output = ra1vat[['test']]
    output.to_csv(r'\\Acn047oslfsv04\s\srvlload\ST' + str(year[-2:])+str(week) + '.RA1', index=False, header=False, quoting=3,
                  sep=';', encoding='ansi')
    print('Przerobilem RA1 dla ST')
