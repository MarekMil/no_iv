import db_connect as db  # module for db connect
import pandas as pd  # data manipulation library
import numpy as np  # data manipulation library
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
from sklepy import dniMiesiace, period_update
con = db.engine.connect()  # connection db


period = period_update()
country_id = 'no'  # country id

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

# SQL query
SQL_statement = '''select unique ac_nshopid from vldprocess_{country_id}.storemonitor
where nc_periodid = {period}
and ac_dtgroup = 'DISPLAY_{dtcountry}'
and ac_monitortag = 103
'''

# adjusting SQL query: change period, country
SQL_statement = SQL_statement.format(period=period, dtcountry=country_id.upper(), country_id=country_id)

outpt2 = con.execute(SQL_statement)  # execution query
df2 = pd.DataFrame(outpt2.fetchall())  # fetch data and covertion to Data Frame
df2.columns = outpt2.keys()  # set columns names
df2.ac_nshopid = df2.ac_nshopid.astype(float)  # convert column nshopid to float


# function with conditional to check status of store
def my_test(active, loaded, source=''):
    if active == 'YES' and not (loaded > 0):
        if source == 'XCOPY':  # if stores is active and source is XCOPY
            return 'Display data; Perm_Xcopy'
        else:  # if stores is active and source is other
            return 'Missing display data'
    elif active == 'YES' and loaded > 0:  # if stores is active and data is loaded
        return 'Display data'
    else:   # if stores is not active
        return 'Wrong MTNR number'


if country_id == 'no':  # Norwegian part
    path = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\\'
    file_name='MissingDisplay_{period}.xlsx'.format(period = year + week)
    df2['ac_nshopid'] = df2['ac_nshopid'] - 1800000000  # change nshopid to MTNR
    file = r'\\acn047oslfsv05\statavd\Sample\Sample.xlsx'  # path with Sample file

    # load Sample and filter necessary columns
    data = pd.ExcelFile(file)
    print(data.sheet_names)
    df1 = data.parse('Sample')

    # outer join sample with output from query
    display = pd.merge(
        df1[['MTNR', 'ActiveInCausalSample', 'HHT', 'TELLERNR', 'Butnavn2', 'Adresse', 'Postnr', 'Poststed']], df2,
        left_on='MTNR', right_on='ac_nshopid', how='outer')

    # adding column with status
    display['status'] = display.apply(lambda row: my_test(row['ActiveInCausalSample'], row['ac_nshopid']), axis=1)

    # filter for loaded stores and active stores
    filtr = np.logical_or(display['ac_nshopid'].notnull(), display['ActiveInCausalSample'] == 'YES')
    display = display[filtr]

    # output to console
    print(display[np.logical_or(display['status'] == 'Missing display data', display['status'] == 'Wrong MTNR number')][
              ['ac_nshopid', 'status']])

    # sort status by status to see missing stores on the top of the list
    display = display.sort_values(by='status', ascending=False)[
        ['MTNR', 'HHT', 'TELLERNR', 'Butnavn2', 'Adresse', 'Postnr', 'Poststed', 'status']]

elif country_id == 'dk':  # Danish part for test. Only path ad filtered colums are different
    file_name = r'C:\Users\olwo7001\Desktop\SIRVAL_Denmark\Display\MissingDisplay_{period}.xlsx'.format(
        period=str(period + 199818))
    df2['ac_nshopid'] = df2['ac_nshopid'] - 7800000000

    # file = r'Y:\Sample\sample_DK.xlsx'
    file = r'C:\Users\olwo7001\Desktop\SIRVAL_Denmark\Display\sample_DK.xlsx'
    data = pd.ExcelFile(file)
    print(data.sheet_names)
    df1 = data.parse('sample_dk')
    display = pd.merge(
        df1[['SHOP', 'ActiveInCausalSample', 'Source', 'name', 'address', 'postcode', 'postdist']], df2,
        left_on='SHOP', right_on='ac_nshopid', how='outer')
    display['status'] = display.apply(lambda row: my_test(row['ActiveInCausalSample'], row[
        'ac_nshopid'], row['Source']), axis=1)

    filtr = np.logical_or(display['ac_nshopid'].notnull(), display['ActiveInCausalSample'] == 'YES')
    display = display[filtr]

    print(display[np.logical_or(display['status'] == 'Missing display data', display['status'] == 'Wrong MTNR number')][
              ['ac_nshopid', 'status']])

    display = display.sort_values(by='status', ascending=False)[
        ['SHOP', 'Source', 'name', 'address', 'postcode', 'postdist', 'status']]

# export to excel file
display.to_excel(path+file_name, 'QUESTIONS', index=False)

try:
    gmail_user = 'marek.milcarz@nielsen.com'
    to_addr = ['malgorzata.mergner@nielsen.com', 'marek.milcarz@nielsen.com','nielsenoperationscenterwarsawnordicsbauiv@nielsen.com',
               'Linda.Hakonsen@nielsen.com','Margareta.Backman@nielsen.com','msci.norway@nielsen.com','katarzyna.grochowska@nielsen.com']
    # to_addr = ['marek.milcarz@nielsen.com','katarzyna.grochowska@nielsen.com']
    gmail_password = 'knkouhzxasxywufh'

    msg = MIMEMultipart()
    msg['From'] = gmail_user
    msg['To'] = ", ".join(to_addr)
    msg['Subject'] = 'Missing causal shops week ' + str(week)
    body = '''
Hi,

Please find the missing causal shops for week {week} attached.

Regards.'''.format(week=str(week))

    attachment = open(path + file_name, 'rb')

    p = MIMEBase('application', 'octet-stream')
    p.set_payload((attachment).read())
    encoders.encode_base64(p)
    p.add_header('Content-Disposition', 'attachment; filename = %s' % file_name)

    msg.attach(MIMEText(body, 'plain'))
    msg.attach(p)

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)

    text = msg.as_string()

    server.sendmail(gmail_user, to_addr, text)
    server.quit()
except:
    print('Coś poszło nie tak z wysyłaniem maila. Plik powinienn byc na dysku')
