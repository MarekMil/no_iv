import pandas as pd
import banner_plot as bp
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import db_connect as db
from sklepy import period_update,dniMiesiace

con = db.engine.connect()

#automatyczny update periodu
period = period_update()

country_id = 'no'

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

# period = input('Production period: ')
transistion_period = '1917'
shops = '''('1800001005',
'1800001354',
'1800005123',
'1800007584',
'1800007661',
'1800009387',
'1800009390',
'1800009789',
'1800009838',
'1800001342',
'1800001343',
'1800001350',
'1800001351',
'1800001352',
'1800001408',
'1800003649',
'1800003650',
'1800003651',
'1800003652',
'1800003653',
'1800003654',
'1800003655',
'1800003657',
'1800003658',
'1800003662',
'1800003663',
'1800003664',
'1800003665',
'1800003666',
'1800003667',
'1800003668',
'1800003669',
'1800003671',
'1800003674',
'1800003675',
'1800003715',
'1800003721',
'1800003724',
'1800003728',
'1800003729',
'1800003731',
'1800003733',
'1800003735',
'1800003736',
'1800003749',
'1800003754',
'1800003756',
'1800003757',
'1800003831',
'1800004522',
'1800004523',
'1800004524',
'1800004526',
'1800004527',
'1800004528',
'1800004529',
'1800004530',
'1800004531',
'1800004532',
'1800004533',
'1800004534',
'1800004535',
'1800004536',
'1800004537',
'1800004538',
'1800004539',
'1800004540',
'1800004541',
'1800004542',
'1800004543',
'1800004544',
'1800004545',
'1800004548',
'1800004549',
'1800004550',
'1800004551',
'1800004553',
'1800004555',
'1800004556',
'1800004557',
'1800004558',
'1800004559',
'1800004560',
'1800004561',
'1800004562',
'1800004563',
'1800004565',
'1800004566',
'1800004567',
'1800004569',
'1800004570',
'1800004571',
'1800004574',
'1800004575',
'1800004577',
'1800004578',
'1800004580',
'1800004581',
'1800004582',
'1800004583',
'1800004584',
'1800004586',
'1800004587',
'1800004590',
'1800004591',
'1800004593',
'1800004594',
'1800004596',
'1800004597',
'1800004598',
'1800004599',
'1800004600',
'1800004601',
'1800004602',
'1800004604',
'1800004605',
'1800004606',
'1800004610',
'1800004612',
'1800004614',
'1800004615',
'1800004616',
'1800004617',
'1800004618',
'1800004619',
'1800004620',
'1800004623',
'1800004624',
'1800004625',
'1800004626',
'1800004628',
'1800004629',
'1800004630',
'1800004631',
'1800004632',
'1800004633',
'1800004634',
'1800004636',
'1800004637',
'1800004638',
'1800004639',
'1800004641',
'1800004643',
'1800004644',
'1800004645',
'1800004648',
'1800004650',
'1800004651',
'1800004652',
'1800004653',
'1800004654',
'1800004656',
'1800004657',
'1800004659',
'1800004660',
'1800004662',
'1800004663',
'1800004664',
'1800004665',
'1800004666',
'1800004667',
'1800004668',
'1800004669',
'1800004670',
'1800004671',
'1800004673',
'1800004674',
'1800004675',
'1800004676',
'1800004679',
'1800004681',
'1800004683',
'1800004686',
'1800004687',
'1800004688',
'1800004689',
'1800004691',
'1800004693',
'1800004697',
'1800004699',
'1800004701',
'1800004702',
'1800004706',
'1800004707',
'1800004710',
'1800004711',
'1800004713',
'1800004715',
'1800004716',
'1800004719',
'1800004720',
'1800004722',
'1800004723',
'1800004724',
'1800004725',
'1800004726',
'1800004727',
'1800004730',
'1800004731',
'1800004733',
'1800004738',
'1800004740',
'1800004741',
'1800004742',
'1800004743',
'1800004745',
'1800004746',
'1800004755',
'1800004757',
'1800004758',
'1800004760',
'1800004761',
'1800004762',
'1800004763',
'1800004764',
'1800004765',
'1800004766',
'1800004767',
'1800004768',
'1800004769',
'1800004771',
'1800004813',
'1800004821',
'1800004880',
'1800005484',
'1800005485',
'1800005486',
'1800005489',
'1800005763',
'1800005764',
'1800005766',
'1800005767',
'1800005768',
'1800005771',
'1800005774',
'1800005910',
'1800006590',
'1800006840',
'1800006848',
'1800006852',
'1800006854',
'1800006855',
'1800006858',
'1800006859',
'1800006861',
'1800006862',
'1800006863',
'1800006907',
'1800006910',
'1800006911',
'1800006912',
'1800006913',
'1800006914',
'1800006991',
'1800006996',
'1800006997',
'1800007001',
'1800007002',
'1800007003',
'1800007004',
'1800007007',
'1800007009',
'1800007010',
'1800007014',
'1800007021',
'1800007025',
'1800007026',
'1800007028',
'1800007029',
'1800007030',
'1800007031',
'1800007034',
'1800007035',
'1800007036',
'1800007038',
'1800007040',
'1800007041',
'1800007042',
'1800007043',
'1800007045',
'1800007046',
'1800007047',
'1800007049',
'1800007050',
'1800007051',
'1800007052',
'1800007053',
'1800007054',
'1800007056',
'1800007057',
'1800007058',
'1800007059',
'1800007062',
'1800007217',
'1800007357',
'1800007421',
'1800007428',
'1800007476',
'1800007477',
'1800007478',
'1800007479',
'1800007510',
'1800007515',
'1800007657',
'1800007664',
'1800007790',
'1800007795',
'1800007796',
'1800007801',
'1800008039',
'1800008040',
'1800008800',
'1800009529',
'1800009530',
'1800009531',
'1800009532',
'1800009533',
'1800009534',
'1800009535',
'1800009536',
'1800009949',
'1800009950',
'1800007000',
'1800009111',
'1800007622',
'1800005801',
'1800007812',
'1800004940',
'1800005463',
'1800006669',
'1800005285',
'1800005602',
'1800004372',
'1800004287',
'1800004918',
'1800002023',
'1800008047',
'1800008862',
'1800009524',
'1800004735',
'1800008996',
'1800006909',
'1800007006',
'1800007482',
'1800009528',
'1800005488',
'1800005716',
'1800006864',
'1800007959',
'1800008693')'''

#dir_out = eg.diropenbox(msg=None, title=None, default=r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week))
file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\ShopsToChecks_{period}.pdf'.format(period=period)
#file_name = r'C:\Users\olwo7001\Desktop\SIRVAL_Norway\2018_27_2009\volumetric\ShopsToChecks_{period}.pdf'.format(period=period)
pdf = matplotlib.backends.backend_pdf.PdfPages(file_name)

SQL_statement = '''
select ( CASE WHEN cy.ac_periodlabel is NULL THEN  ly.ac_periodlabel ELSE cy.ac_periodlabel END) as ac_periodlabel,
( CASE WHEN cy.periodid is NULL THEN  ly.periodid ELSE cy.periodid END) as periodid,
( CASE WHEN cy.ac_nshopid is NULL THEN  ly.ac_nshopid ELSE cy.ac_nshopid END) as ac_nshopid,
cy.retailer, cy.items as n_records, cy.value as n_value, ly.items  as old_records, ly.value as old_value from
(select distinct ac_periodlabel, m.nc_periodid as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='TOT_UNITS' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='TOT_VALUES' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in  {shops}
and ac_dtgroup='VOLUMETRIC' and (p.nc_periodid between {period} - 52 and {period})  and p.nc_periodid>{tr_period} group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
UNION
select distinct ac_periodlabel, m.nc_periodid as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='SUM_SLOT1' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='SUM_SLOT2' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in {shops}
 and ac_dtgroup='VOL_RCC' and (p.nc_periodid between {period} - 52 and {period})  and p.nc_periodid<={tr_period}  group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
) cy
FULL OUTER JOIN
(select distinct ac_periodlabel, m.nc_periodid+52 as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='TOT_UNITS' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='TOT_VALUES' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in  {shops}
 and ac_dtgroup='VOLUMETRIC' and (p.nc_periodid between {period} - 104 and {period} - 52)  and p.nc_periodid>{tr_period} group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
UNION
select distinct ac_periodlabel, m.nc_periodid+52 as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='SUM_SLOT1' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='SUM_SLOT2' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in {shops}
 and ac_dtgroup='VOL_RCC' and (p.nc_periodid between {period} - 104 and {period} - 52) and p.nc_periodid<={tr_period}  group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
) ly
ON cy.periodid=ly.periodid and cy.ac_nshopid = ly.ac_nshopid
ORDER BY ac_nshopid, periodid'''

SQL_statement = SQL_statement.format(country=country_id, period=period, tr_period=transistion_period, shops=shops)

outpt = con.execute(SQL_statement)
df = pd.DataFrame(outpt.fetchall())
df.columns = outpt.keys()
print(df)
con.close()
df.n_value = df.n_value.astype(float)
df.old_value = df.old_value.astype(float)
df.ac_periodlabel = df.ac_periodlabel.astype("category")

print(df)


for store in df['ac_nshopid'].unique():
    print(store)
    df1 = df[df.ac_nshopid == store]
    df1[['n_value', 'n_records', 'old_value', 'old_records']] = df1[['n_value', 'n_records', 'old_value', 'old_records']].interpolate()
    #print(df1.head())
    #print(df1.info())
    fig1, ax1 = plt.subplots()
    shopt, typ = bp.banner_p(country_id, str(df1.retailer[df1.retailer.notnull()].unique()[0]))
    #shopt, typ='',''
    title = 'Banner={retailer}  AC_NSHOPID={shop} \n index = {index}  type = {typ} \n dashed lines = last year'.format(retailer=str(df1.retailer[df1.retailer.notnull()].unique()[0]), shop=store, index=shopt, typ=typ)
    plt.title(title, fontsize=7)
    #plt.suptitle('\n dashed lines = last year\n', fontsize=10)
    plt.xticks(df1['periodid'], fontsize=5, rotation=90)
    plt.yticks(fontsize=6)
    plt.grid(b=True, which='both', linewidth=0.2)
    color = 'tab:red'
    ax1.set_xlabel('Weeks')
    ax1.set_ylabel('Sales value', color=color)
    ax1.plot(df1['periodid'], df1['n_value'], color='red', linewidth=0.75, marker='.', markersize=1)
    ax1.plot(df1['periodid'], df1['old_value'], color='red', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.set_xticklabels(df1['ac_periodlabel'])
    ax1.set_ylim(ymin=0)

    ax2 = ax1.twinx()
    plt.yticks(fontsize=6)
    color = 'tab:green'
    ax2.set_ylabel('Records', color=color)  # we already handled the x-label with ax1
    ax2.plot(df1['periodid'], df1['n_records'], color='green', linewidth=0.75, marker='.', markersize=1)
    ax2.plot(df1['periodid'], df1['old_records'], color='green', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_ylim(ymin=0)

    fig1.tight_layout()  # otherwise the right y-label is slightly clipped


    pdf.savefig(fig1)
    plt.clf()
    plt.close()


pdf.close()
