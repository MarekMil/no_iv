import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import db_connect as db
import db_connect_Valprda as dbV
from sklepy import period_update, dniMiesiace
#commnet
con = db.engine.connect()
country_id = 'no'
country_code = '''('NO')'''
con = db.engine.connect()

SQL_statement ='''select max( nc_periodid) from VLDINSTR_NO.PERIODS where  nc_periodopen = 1'''


period = period_update()

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

transistion_period = '1917'
channel = '''('NOSCAN')'''
sample_ids = '''(
164,1000189
)
'''
retailers = ('C1:COOP EXTRA', 'C1:COOP OBS', 'C2:COOP MARKED','C4:COOP EXTRA','C4:COOP MEGA','C6:COOP OBS','C6:COOP PRIX','C7:COOP MARKED','C7:COOP OBS')
nr_periods_back = 52

file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\BannerChecks_{period}.pdf'.format(period=period)
pdf = matplotlib.backends.backend_pdf.PdfPages(file_name)

SQL_statement = '''
select ( CASE WHEN cy.ac_periodlabel is NULL THEN  ly.ac_periodlabel ELSE cy.ac_periodlabel END) as ac_periodlabel,
( CASE WHEN cy.periodid is NULL THEN  ly.periodid ELSE cy.periodid END) as periodid,
( CASE WHEN cy.ac_nshopid is NULL THEN  ly.ac_nshopid ELSE cy.ac_nshopid END) as ac_nshopid,
( CASE WHEN cy.retailer is NULL THEN  ly.retailer ELSE cy.retailer END) as retailer,
cy.items as n_records, cy.value as n_value, cy.sumunits as  n_units, ly.sumunits as old_units, ly.items  as old_records, ly.value as old_value from
(select distinct ac_periodlabel, m.nc_periodid as periodid, 
Case when m.ac_NSHOPID in ('1800003336', '1800001432', '1800001749') then 'NG:CC_MARTIN' else ac_retailer END as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='TOT_UNITS' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='TOT_VALUES' then nc_statvalue end) as value
from vldprocess_{country_id}.storestats m,
     vldinstr_{country_id}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL')
and s.ac_retailer <> ' ' /*{retailers}*/
and ac_dtgroup='VOLUMETRIC' and (p.nc_periodid between {period} - {nr_periods_back} and {period})  and p.nc_periodid>{transistion_period} group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
UNION
select distinct ac_periodlabel, m.nc_periodid as periodid,
 Case when m.ac_NSHOPID in ('1800003336', '1800001432', '1800001749') then 'NG:CC_MARTIN' else ac_retailer END as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='SUM_SLOT1' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='SUM_SLOT2' then nc_statvalue end) as value
from vldprocess_{country_id}.storestats m,
     vldinstr_{country_id}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL')
and s.ac_retailer <> ' ' /*{retailers}*/
 and ac_dtgroup='VOL_RCC' and (p.nc_periodid between {period} - {nr_periods_back} and {period})  and p.nc_periodid<={transistion_period}  group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
) cy
LEFT OUTER JOIN
(select distinct ac_periodlabel, m.nc_periodid+52 as periodid, 
Case when m.ac_NSHOPID in ('1800003336', '1800001432', '1800001749') then 'NG:CC_MARTIN' else ac_retailer END as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='TOT_UNITS' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='TOT_VALUES' then nc_statvalue end) as value
from vldprocess_{country_id}.storestats m,
     vldinstr_{country_id}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL')
 and ac_dtgroup='VOLUMETRIC' and (p.nc_periodid between {period}-52-{nr_periods_back} and {period}-52)  and p.nc_periodid>{transistion_period} group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
UNION
select distinct ac_periodlabel, m.nc_periodid+52 as periodid, Case when m.ac_NSHOPID in ('1800003336', '1800001432', '1800001749') then 'NG:CC_MARTIN' else ac_retailer END as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='SUM_SLOT1' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='SUM_SLOT2' then nc_statvalue end) as value
from vldprocess_{country_id}.storestats m,
     vldinstr_{country_id}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL')
 and ac_dtgroup='VOL_RCC' and (p.nc_periodid between {period}-52-{nr_periods_back} and {period} - 52) and p.nc_periodid<={transistion_period}  group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
) ly
ON cy.periodid=ly.periodid and cy.ac_nshopid = ly.ac_nshopid
ORDER BY ac_periodlabel'''

SQL_statement = SQL_statement.format(country_id=country_id, period=period, transistion_period=transistion_period, nr_periods_back=nr_periods_back, retailers=retailers)
print(SQL_statement)

outpt = con.execute(SQL_statement)
df = pd.DataFrame(outpt.fetchall())
df.columns = outpt.keys()
con.close()
#print(df.head())

con2 = dbV.engine.connect()
SQL_statement2 = '''
  select sho_external_code as ac_nshopid, (ssw_tpr_id+989) as periodid
  from MADRAS_DATA.TMXP_SHOP_SAMPLE a
  INNER JOIN
  MADRAS_DATA.trsh_shop b
  ON a.ssw_sho_id=b.sho_id
  where a.SSW_CCH_ID in {channel} and a.SSW_SAM_ID in {sample_ids} and b.sho_cch_cou_code in {country_code} and a.ssw_tpr_id between {period}-989-{nr_periods_back} and {period}-989
'''
SQL_statement2 = SQL_statement2.format(channel=channel, country_code=country_code, period=period, nr_periods_back=nr_periods_back, sample_ids=sample_ids)

outpt2 = con2.execute(SQL_statement2)
df2 = pd.DataFrame(outpt2.fetchall())
df2.columns = outpt2.keys()
#print(df2.head())
con2.close()

df = pd.merge(df, df2,  how='inner', left_on=['periodid', 'ac_nshopid'], right_on=['periodid', 'ac_nshopid'])
#print(df.head())
df.n_value = df.n_value.astype(float)
df.old_value = df.old_value.astype(float)
#print(df.info())
df['retailer'] = df['retailer'].apply(lambda x: x[(x.find(':')+1):])
df = df.groupby(["ac_periodlabel", "periodid", "retailer"]).aggregate({'ac_nshopid':'count', 'n_records':np.sum, 'n_value':np.sum, 'old_records':np.sum, 'old_value':np.sum,"n_units":np.sum,'old_units':np.sum})
#print(df.head())
#df = pd.pivot_table(df, index=["ac_periodlabel", "periodid", "retailer"], values=['n_records', 'n_value', 'old_records', 'old_value'] , aggfunc=np.sum)
df = df.reset_index()
#print(df.head())

#df.n_value = df.n_value.astype(float)
#df.old_value = df.old_value.astype(float)
#df.retailer = df.retailer.astype("str")
#df.ac_periodlabel = df.retailer.astype("str")
df = df.sort_values(by=["periodid","retailer"])
#df.ac_periodlabel = df.ac_periodlabel.astype("category")

#filtr = np.logical_and(df['n_records'].notnull(), df['old_records'].notnull())
#df[['n_value', 'n_records', 'old_value', 'old_records']] = df[['n_value', 'n_records', 'old_value', 'old_records']].interpolate()


for retailer in df['retailer'].unique():

    if retailer is not None:
        print(retailer)
        print(type(retailer))

        df1 = df[df.retailer == retailer]
        #print(df1.head())
        #print(df1.info())
        fig1, ax1 = plt.subplots()


        title = 'Banner={retailer} \n Stores in sample={Count} dashed lines = last year'.format(retailer=str(df1.retailer.unique()), Count=df1[df1.periodid == period-1].ac_nshopid.unique())
        plt.title(title, fontsize=7)
        #plt.suptitle('\n dashed lines = last year\n', fontsize=10)
        plt.xticks(df1['periodid'], fontsize=5, rotation=90)
        plt.yticks(fontsize=6)
        plt.grid(b=True, which='both', linewidth=0.2)
        color = 'tab:red'
        ax1.set_xlabel('Weeks')
        ax1.set_ylabel('Sales value', color=color)
        ax1.plot(df1['periodid'], df1['n_value'], color='red', linewidth=0.75, marker='.', markersize=1)
        ax1.plot(df1['periodid'], df1['old_value'], color='red', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
        ax1.tick_params(axis='y', labelcolor=color)
        ax1.set_xticklabels(df1['ac_periodlabel'])
        ax1.set_ylim(ymin=0)
        #ax1.set_yscale("symlog")

        ax2 = ax1.twinx()
        plt.yticks(fontsize=6)
        color = 'tab:green'
        ax2.set_ylabel('Records', color=color)  # we already handled the x-label with ax1
        ax2.plot(df1['periodid'], df1['n_records'], color='green', linewidth=0.75, marker='.', markersize=1)
        ax2.plot(df1['periodid'], df1['old_records'], color='green', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
        ax2.tick_params(axis='y', labelcolor=color)
        ax2.set_ylim(ymin=0)


        # plt.yticks(fontsize=6)
        #
        # ax2.plot(df1['periodid'], df1['n_units'], color='black', linewidth=0.75, marker='.', markersize=1)
        # ax2.plot(df1['periodid'], df1['old_units'], color='black', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
        # ax2.tick_params(axis='y', labelcolor=color)
        # ax2.set_ylim(ymin=0)

        fig1.tight_layout()  # otherwise the right y-label is slightly clipped


        pdf.savefig(fig1)


pdf.close()

'''
print('aksssssssss')
print(np.size(df.ac_periodlabel))
print('aksssssssss')
print(np.size(df.n_records))
'''

#plt.show()
