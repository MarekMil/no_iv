from sklepy import dniMiesiace, period_update
import db_connect as db
import pandas as pd
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib

con = db.engine.connect()

#automatyczny update periodu
period = period_update()

country_id = 'no'

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']



sql_check1='''
select * from (select distinct  r.nc_periodid, s.ac_ibd as FeatureGroup,(case when ac_ibd = '12' then 'COOP PRIX' else
case when ac_ibd = '13' then 'COOP EXTRA' else
case when ac_ibd = '15' then 'COOP SMART CLUB' else
case when ac_ibd = '17' then 'COOP MARKED' else
case when ac_ibd = '18' then 'MATKROKEN' else
case when ac_ibd = '19' then 'COOP NAERKJOEB' else
case when ac_ibd = '21' then 'RIMI' else
case when ac_ibd = '22' then 'ICA SUPER' else
case when ac_ibd = '23' then 'ICA NAER' else
case when ac_ibd = '24' then 'ICA MAXI' else
case when ac_ibd = '29' then 'ICA MATKROKEN' else
case when ac_ibd = '71' then 'BUNNPRIS' else
case when ac_ibd = '72' then 'SPAR and EUROSPAR' else
case when ac_ibd = '75' then 'KIWI' else
case when ac_ibd = '76' then 'MENY' else
case when ac_ibd = '78' then 'JOKER' else
case when ac_ibd = '1101' then 'COOP OBS 1' else
case when ac_ibd = '1102' then 'COOP OBS 2' else
case when ac_ibd = '1103' then 'COOP OBS 3' else
case when ac_ibd = '1104' then 'COOP OBS 4' else
case when ac_ibd = '1105' then 'COOP OBS 5' else
case when ac_ibd = '1106' then 'COOP OBS 6' else
case when ac_ibd = '1107' then 'COOP OBS 7' else
case when ac_ibd = '1108' then 'COOP OBS 8' else
case when ac_ibd = '1109' then 'COOP OBS 9' else
case when ac_ibd = '1110' then 'COOP OBS 10' else
case when ac_ibd = '1111' then 'COOP OBS 11' else
case when ac_ibd = '141' then 'COOP MEGA MIDT' else
case when ac_ibd = '142' then 'COOP MEGA NORD' else
case when ac_ibd = '143' then 'COOP MEGA VEST' else
case when ac_ibd = '144' then 'COOP MEGA SYD' else
case when ac_ibd = '145' then 'COOP MEGA VEST - BERGEN' else
case when ac_ibd = '3101' then 'REMA_TRONDHEIM' else
case when ac_ibd = '3102' then 'REMA_?VRIG MIDT NORGE' else
case when ac_ibd = '3103' then 'REMA_NORD' else
case when ac_ibd = '3104' then 'REMA_MIDTRE ?STLAND' else
case when ac_ibd = '3105' then 'REMA_NORDLAND' else
case when ac_ibd = '3106' then 'REMA_BERGEN' else
case when ac_ibd = '3107' then 'REMA_VESTRE ?STLAND' else
case when ac_ibd = '3108' then 'REMA_OSLO' else
case when ac_ibd = '3109' then 'REMA_NORDRE ?STLAND' else
case when ac_ibd = '3110' then 'REMA_S?R' else
case when ac_ibd = '3111' then 'REMA_S?R VEST' else
case when ac_ibd = '3112' then 'REMA_?VRIG VEST' else
case when ac_ibd = '3113' then 'REMA_NORDRE VESTLAND' else
case when ac_ibd = '3114' then 'REMA_?STRE ?STLAND' else null End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End End)
as FeaturegroupDescription,r.ac_nshopid from vldrawdata_no.rawdata r, vldinstr_no.stores s
where r.ac_nshopid = s.ac_nshopid
and r.ac_dtgroup = 'FEATURE_NO'
and r.nc_periodid in  ({earlier_Period}, {period}))
pivot (count(ac_nshopid) for nc_periodid in ({earlier_Period}, {period}))'''.format(earlier_Period=str(period-1),period=str(period))
outpt_check1 = con.execute(sql_check1)
df_check1 = pd.DataFrame(outpt_check1.fetchall())
df_check1.columns = outpt_check1.keys()


sql_check2='''select * from (select distinct  r.nc_periodid, s.ac_icell as FeatureGroup,(case when ac_icell = '12' then 'COOP PRIX' else
case when ac_icell = '13' then 'COOP EXTRA' else
case when ac_icell= '15' then 'COOP SMART CLUB' else
case when ac_icell = '17' then 'COOP MARKED' else
case when ac_icell = '18' then 'MATKROKEN' else
case when ac_icell = '19' then 'COOP NAERKJOEB' else
case when ac_icell = '21' then 'RIMI' else
case when ac_icell = '22' then 'ICA SUPER' else
case when ac_icell = '23' then 'ICA NAER' else
case when ac_icell = '24' then 'ICA MAXI' else
case when ac_icell = '29' then 'ICA MATKROKEN' else
case when ac_icell = '71' then 'BUNNPRIS' else
case when ac_icell = '73' then 'EUROSPAR' else
case when ac_icell = '72' then 'SPAR' else
case when ac_icell = '75' then 'KIWI' else
case when ac_icell = '76' then 'MENY' else
case when ac_icell = '78' then 'JOKER' else
case when ac_icell = '11' then 'COOP OBS' else
case when ac_icell = '14' then 'COOP MEGA' else
case when ac_icell = '31' then 'REMA' else null End End End End End End End End End End End End End End End End End End End End)
as Retailer,r.ac_nshopid from rawdata r, stores s
where r.ac_nshopid = s.ac_nshopid
and r.ac_dtgroup = 'TVSPOT_NO'
and r.nc_periodid = {period})
pivot (count(ac_nshopid) for nc_periodid in ({period}))'''.format(period=str(period))
outpt_check2 = con.execute(sql_check2)
df_check2 = pd.DataFrame(outpt_check2.fetchall())
df_check2.columns = outpt_check2.keys()

SQL_statement_to_xls = '''
with desc_period as (select max(NC_PERIODINSERTED) as period , ac_cref from 
descriptions group by ac_cref)
select distinct  r.nc_periodid, s.ac_ibd as FeatureGroup,r.ac_cref,r.ac_creftype,nvl(d.AC_CREFDESCRIPTION,'NA') as AC_CREFDESCRIPTION from rawdata r, 
(select * from descriptions
where nc_hash_signature!=1882293593) d, 
stores s, desc_period t 
where r.ac_cref = d.ac_cref
and d.ac_cref = t.ac_cref
and r.ac_nshopid = s.ac_nshopid
and r.ac_dtgroup = 'FEATURE_NO'
and r.nc_periodid = {period}
and d.NC_PERIODINSERTED= t.period

'''.format(period=period)

dict = {'12': 'COOP PRIX', '13': 'COOP EXTRA', '15': 'COOP SMART CLUB', '17': 'COOP MARKED', '18': 'MATKROKEN',
        '19': 'COOP NAERKJOEB', '21': 'RIMI', '22': 'ICA SUPER', '23': 'ICA NAER', '24': 'ICA MAXI',
        '29': 'ICA MATKROKEN',
        '71': 'BUNNPRIS', '72': 'SPAR and EUROSPAR', '75': 'KIWI', '76': 'MENY', '78': 'JOKER',
        '1101': 'COOP OBS 1',
        '1102': 'COOP OBS 2', '1103': 'COOP OBS 3', '1104': 'COOP OBS 4', '1105': 'COOP OBS 5',
        '1106': 'COOP OBS 6',
        '1107': 'COOP OBS 7', '1108': 'COOP OBS 8', '1109': 'COOP OBS 9', '1110': 'COOP OBS 10',
        '1111': 'COOP OBS 11',
        '141': 'COOP MEGA MIDT', '142': 'COOP MEGA NORD', '143': 'COOP MEGA VEST', '144': 'COOP MEGA SYD',
        '145': 'COOP MEGA VEST - BERGEN', '3101': 'REMA_TRONDHEIM', '3102': 'REMA_ŘVRIG MIDT NORGE',
        '3103': 'REMA_NORD',
        '3104': 'REMA_MIDTRE ŘSTLAND', '3105': 'REMA_NORDLAND', '3106': 'REMA_BERGEN',
        '3107': 'REMA_VESTRE ŘSTLAND',
        '3108': 'REMA_OSLO', '3109': 'REMA_NORDRE ŘSTLAND', '3110': 'REMA_SŘR', '3111': 'REMA_SŘR VEST',
        '3112': 'REMA_ŘVRIG VEST', '3113': 'REMA_NORDRE VESTLAND', '3114': 'REMA_ŘSTRE ŘSTLAND',
        }

group = pd.DataFrame.from_dict(dict, orient='index')
group = group.reset_index()
group.columns = ['IBD', 'FeaturegroupDescription']
outpt_to_xls = con.execute(SQL_statement_to_xls)
sql = pd.DataFrame(outpt_to_xls.fetchall())
sql.columns = outpt_to_xls.keys()
df_xls = pd.merge(sql, group, how='left', left_on=['featuregroup'], right_on=['IBD'])

con.close()

file_name = 'Feature_Wk' + str(week) + '.xlsx'
path = r'G:\Team Drives\NO BAU IV\IV_production\\' + str(year) + 'w' + str(week) + '\\'
df_xls.to_excel(path + file_name, 'Sheet1', index=False, )

if all(abs(row[str(period)]-row[str(period-1)])<3 for column, row in df_check1.iterrows()) \
        and all(row[str(period)] != 0 for column, row in df_check1.iterrows()) \
        and len([row[str(period)] for column,row in df_check2.iterrows() if row[str(period)]==1])<1:
    zgoda='y'
else:
    print('\nShop count check:')
    print(df_check1)
    print('\nTV-Spot check:')
    print(df_check2)
    zgoda = input('\nWysłać maila? (y/n):')

if zgoda == 'y':
    gmail_user = 'marek.milcarz@nielsen.com'
    to_addr = ['hanna.magnusson.consultant@nielsen.com', 'malgorzata.mergner@nielsen.com', 'marek.milcarz@nielsen.com','nielsenoperationscenterwarsawnordicsbauiv@nielsen.com',
               'katarzyna.grochowska@nielsen.com', 'steinar.johannessen@nielsen.com', 'sriharsha.dogiparthi.ap@nielsen.com']
    #to_addr = ['marek.milcarz@nielsen.com','katarzyna.grochowska@nielsen.com']
    gmail_password = 'knkouhzxasxywufh'

    msg = MIMEMultipart()
    msg['From'] = gmail_user
    msg['To'] = ", ".join(to_addr)
    msg['Subject'] = 'Feature Sirpair for Norway'+str(week)
    body = '''
Hello Hanna,

Feature data was loaded successfully for Wk{week}.
Kindly find features loaded inside SirVal.

Let us know if require more information.'''.format(week=str(week))

    attachment = open(path + file_name, 'rb')

    p = MIMEBase('application', 'octet-stream')
    p.set_payload((attachment).read())
    encoders.encode_base64(p)
    p.add_header('Content-Disposition', 'attachment; filename = %s' % file_name)

    msg.attach(MIMEText(body, 'plain'))
    msg.attach(p)

    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)

    text = msg.as_string()

    server.sendmail(gmail_user, to_addr, text)
    server.quit()


else:
    print('Zakończyłem pracę.')
