import db_connect as db
import pandas as pd
import numpy as np
from sklepy import period_update,dniMiesiace

con = db.engine.connect()

country = 'no'
#period = int(input('Production period: '))

#automatyczny update periodu
period = period_update()



dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

periods = ""

file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\StillOkDonor_'+str(year)+str(week)+'.csv'
dir_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)

for i in np.linspace(11, 1, num=11, dtype=int):
    if i == 1:
        periods = periods + str(period-i)
    else:
        periods = periods + str(period-i) + ","
print(periods)

SQL_statement2 = '''
select UNIQUE AC_NSHOPID
from vldinstr_{country}.stores
where AC_SHOPSTATUS = 'REQUIRED' and ac_nshopid not in (
SELECT UNIQUE AC_NSHOPID FROM vldprocess_{country}.STOREMONITOR
WHERE NC_PERIODID = {period}
AND AC_DTGROUP = 'VOLUMETRIC'
AND AC_MONITORTAG = '999999'
AND AC_MONITORSTATUS <> '0'
)
AND AC_RETAILER <> 'SHELL'
'''

SQL_statement2 = SQL_statement2.format(period=period, country=country)

outpt2 = con.execute(SQL_statement2)
df2 = outpt2.fetchall()

stores = ''
for i, x in enumerate(df2):
    if i == len(df2)-1:
        stores = stores + "'" + str(x[0]) + "'"
    else:
        stores = stores + "'" + str(x[0]) + "',"


print(stores)
#stores="'7800006207', '7800006206', '7800008085'"
SQL_statement3 = '''
select * from (SELECT nc_periodid, AC_NSHOPID, AC_MONITORSTATUS
FROM vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'
AND AC_MONITORTAG = '999999'
AND NC_PERIODID in ({period}-11,{period}-10,{period}-9,{period}-8, {period}-7, {period}-6, {period}-5, {period}-4, {period}-3, {period}-2, {period}-1) and ac_NSHOPID in (
{stores}))
'''
SQL_statement3 = SQL_statement3.format(period=period, stores=stores, country=country)

#SQL_statement = SQL_statement.format(country=country_id, shops=shops)


outpt3 = con.execute(SQL_statement3)
#print(outpt3.fetchall())
df3 = pd.DataFrame(outpt3.fetchall())
df3.columns = outpt3.keys()
df3 = pd.pivot_table(df3, values='ac_monitorstatus', index='ac_nshopid', columns=['nc_periodid'], aggfunc=np.sum)


SQL_statement = '''
SELECT ac_nshopid,ac_retailer, (ac_ebd) as EBD, (  
CASE WHEN ac_ebd is NULL THEN NULL ELSE  
CASE WHEN ac_nshopid in (SELECT AC_NSHOPID  
FROM vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'  
AND AC_MONITORTAG = '999999'   
AND NC_PERIODID in ({period}-1) and AC_MONITORSTATUS = 14 ) THEN '14' ELSE /* tu sprawdzam xcopy sprzed tygodnia*/  
CASE WHEN ac_nshopid not in (SELECT AC_NSHOPID  
FROM  vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'  
AND AC_MONITORTAG = '999999'   
AND NC_PERIODID in ({period}-8, {period}-7, {period}-6, {period}-5, {period}-4, {period}-3, {period}-2, {period}-1)) THEN '14_to_check' ELSE /* tu sprawdzam czy sklep nie jest nowy*/  
CASE WHEN ac_ebd = 'CANCELLED' THEN NULL ELSE  
CASE WHEN (ac_ebd = ac_nshopid or ac_ebd like ('EXE%')) and ac_nshopid not in (select ac_nshopid from(  
SELECT ac_nshopid, count(*) as liczba  
FROM  vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'  
AND AC_MONITORTAG = '999999'   
AND NC_PERIODID in ({period}-4, {period}-3, {period}-2, {period}-1) and AC_MONITORSTATUS = 13 group by ac_nshopid) where liczba > 2) THEN '13' ELSE /* tu sprawdzam ebd do tcopy i czy sklep nie by tcopiowany wiecej niz 3 razy*/  
CASE WHEN ac_nshopid in (select ac_nshopid from(  
SELECT ac_nshopid, count(*) as liczba  
FROM  vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'  
AND AC_MONITORTAG = '999999'   
AND NC_PERIODID in ({period}-4, {period}-3, {period}-2, {period}-1) and AC_MONITORSTATUS = 13 group by ac_nshopid) where liczba > 2) THEN '13_to_check' ELSE /* tu sprawdzam czy sklep byl tcopiowany wiecej niz 2 razy i wtedy aplikujemy dodatkowe checki*/  
CASE WHEN ac_nshopid in (select ac_nshopid from(  
SELECT ac_nshopid, count(*) as liczba  
FROM  vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'  
AND AC_MONITORTAG = '999999'   
AND NC_PERIODID in ({period}-8, {period}-7, {period}-6, {period}-5, {period}-4, {period}-3, {period}-2, {period}-1) and AC_MONITORSTATUS = 12 group by ac_nshopid) where liczba > 7) THEN '14' ELSE /* tu sprawdzam czy sklep bul estymowany wiecej niz 7 razy i wtedy aplikujemy xcopy*/  
CASE WHEN ac_nshopid in (select ac_nshopid from(  
SELECT ac_nshopid, count(*) as liczba  
FROM  vldprocess_{country}.STOREMONITOR
WHERE AC_DTGROUP = 'VOLUMETRIC'  
AND AC_MONITORTAG = '999999'   
AND NC_PERIODID in ({period}-4, {period}-3, {period}-2, {period}-1) and AC_MONITORSTATUS = 13 group by ac_nshopid) where liczba > 3) THEN '14_to_check' /* tu sprawdzam czy sklep byl tcopiowany wiecej niz 3 razy i wtedy aplikujemy xcopy*/  
ELSE '12' /* pozostale przypadki na estymacje*/  
End End End End End End End End) As Salvation From vldinstr_{country}.Stores where ac_NSHOPID in (  
{stores})
'''
SQL_statement = SQL_statement.format(period=period, stores=stores, country=country)

#SQL_statement = SQL_statement.format(country=country_id, shops=shops)


outpt = con.execute(SQL_statement)
#print((outpt.fetchall()))
#lista = outpt.fetchall()
df = pd.DataFrame(outpt.fetchall())
print(outpt.keys())
print(df)
df.columns = outpt.keys()

df3 = df3.reset_index()


history = pd.merge(df, df3, left_on='ac_nshopid', right_on='ac_nshopid', how='left')
print(history)

SQL_statement4 = '''
select ac_parameterspecstores, ac_parametervalue as Donor, ac_monitorstatus as Donor_Status from vldsys_{country}.parametervalues pv
INNER JOIN vldprocess_{country}.storemonitor st
ON pv.ac_parametervalue = st.ac_nshopid
where pv.ac_processid in 'XCOPY' and pv.NC_PERACTIVETO in (2139) and pv.ac_parameterspecstores in ({stores})
and st.NC_PERIODID in ({period}) and st.ac_dtgroup in ('VOLUMETRIC') and st.AC_MONITORTAG in ('999999')
'''
SQL_statement4 = SQL_statement4.format(period=period, stores=stores, country=country)

#SQL_statement = SQL_statement.format(country=country_id, shops=shops)

outpt4 = con.execute(SQL_statement4)
out4 = outpt4.fetchall()

print(out4)

if len(out4) == 0:
    print('empty')
    out4 = [(None, None, None)]

df4 = pd.DataFrame(out4)
df4.columns = outpt4.keys()


almost = pd.merge(history, df4, left_on='ac_nshopid', right_on='ac_parameterspecstores', how='left')
print(almost)

if country == 'no':
    still = pd.read_csv(file_name, sep=';', dtype=str, encoding='utf8')
    merged = pd.merge(almost, still, left_on='ac_nshopid', right_on='AC_NSHOPID', how='left')
elif country == 'dk':
    merged = almost


merged = merged.drop_duplicates()
name = dir_name+'\merged.csv'
merged.to_csv(name, sep=';', index=False)
