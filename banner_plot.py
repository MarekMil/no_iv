def banner_p(country, banner):
    if country == 'no':
        if banner == "7-ELEVEN":
            shopt = "Service"
            typ = "Census"
        elif banner == "NARVESEN":
            shopt = "Service"
            typ = "Census"
        elif banner == "SHELL":
            shopt = "Service"
            typ = "Census"
        elif banner == "SHELL/7-ELEVEN":
            shopt = "Service"
            typ = "Census"
        elif banner == "BUNNPRIS":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "COOP EXTRA":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "COOP MARKED":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "COOP MEGA":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "COOP NAERKJOEB":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "COOP OBS":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "COOP PRIX":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "KIWI":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "MATKROKEN":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "MENY":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "REMA 1000":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "RIMI":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "BEST":
            shopt = "Service"
            typ = "Sample"
        elif banner == "MIX":
            shopt = "Service"
            typ = "Sample"
        elif banner == "STATOIL":
            shopt = "Service"
            typ = "Sample"
        elif banner == "YX":
            shopt = "Service"
            typ = "Sample"
        elif banner == "EUROSPAR":
            shopt = "Grocery"
            typ = "Sample"
        elif banner == "JOKER":
            shopt = "Grocery"
            typ = "Sample"
        elif banner == "NG OEVRIGE":
            shopt = "Grocery"
            typ = "Sample"
        elif banner == "SPAR":
            shopt = "Grocery"
            typ = "Sample"
        else:
            shopt = ""
            typ = ""
    elif country == 'dk':
        pos = banner.find(':')+1
        banner = banner[pos:]
        if banner == "BILKA":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "A-Z":
            shopt = "Grocery"
            typ="Census"
        elif banner == "SALLING":
            shopt="Grocery"
            typ="Census"
        elif banner == "FØTEX":
            shopt="Grocery"
            typ="Census"
        elif banner == "FØTEX CONV.":
            shopt="Grocery"
            typ="Census"
        elif banner == "NETTO":
            shopt="Grocery"
            typ="Census"
        elif banner == "KVICKLY":
            shopt="Grocery"
            typ="Census"
        elif banner == "SUPERBRUGSEN":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "DAGLIBRUGSEN":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "BRUGSEN":
            shopt = "Grocery"
            typ = "Census"
        elif banner == "FAKTA":
            shopt = "Grocery"
            typ = "Census"
        elif banner =="IRMA" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="MENY" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="MIN KØBMAND" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="SPAR" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="KIWI" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="REMA 1000" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="LØVBJERG" :
            shopt = "Grocery"
            typ = "Census"
        elif banner =="ALDI":
            shopt = "Grocery"
            typ = "CSSI"
        elif banner =="LIDL":
            shopt = "Grocery"
            typ = "CSSI"
        elif banner =="MATAS":
            shopt = "Drug"
            typ = "Census"
        elif banner =="OK PLUS":
            shopt = "Convenience"
            typ = "Census"
        elif banner =="CIRCLE K":
            shopt = "Convenience"
            typ = "Census"
        elif banner =="Q8 CENTRAL":
            shopt = "Convenience"
            typ = "Census"
        elif banner =="EXTRA":
            shopt = "Convenience"
            typ = "Sample"
        elif banner =="BFI SHELL RBA":
            shopt = "Convenience"
            typ = "Census"
        elif banner =="BFI SHELL DO":
            shopt = "Convenience"
            typ = "Sample"
        elif banner in ("CITY", "TRAIN", "GAS"):
            shopt = "Convenience";
            typ = "Census";
        elif banner =="LETKØB":
            shopt="Convenience"
            typ = "Sample"
        elif banner =="NÆRKØB":
            shopt = "Convenience"
            typ = "Sample"
        else:
            shopt = ""
            typ = ""
        #print(banner)
    return shopt, typ
