import db_connect as db
import pandas as pd
import os
import csv
from sklepy import dniMiesiace, period_update

con = db.engine.connect()

#automatyczny update periodu
period = period_update()


dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']
#period = int(input('Production period: '))
#file_name = r'C:\Users\olwo7001\Desktop\SIRVAL_Norway\2018_21_2003\volumetric\xcopy_corr.txt'

file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\\xcopy_corr.txt'
dir_name = os.path.dirname(file_name)
file_name2 = dir_name+'\est_corr.txt'

SQL_statement = '''
SELECT s.ac_nshopid         AS recepient,
r.nc_acv                  AS recp_acv,
s.ac_monitorstatus        AS donor,
d.nc_acv                  AS donor_acv, 
ROUND(r.nc_acv/d.nc_acv,2)AS ratio 
FROM storemonitor s, 
stores r, 
stores d 
WHERE r.ac_nshopid  =s.ac_nshopid 
AND d.ac_nshopid    = s.ac_monitorstatus 
AND s.ac_monitortag = '700' 
AND s.nc_periodid   = {period}
and s.ac_dtgroup = 'VOL_BUTOM' 
order by ratio
'''
SQL_statement = SQL_statement.format(period=period)

#SQL_statement = SQL_statement.format(country=country_id, shops=shops)


outpt = con.execute(SQL_statement)
#df = pd.DataFrame(outpt.fetchall())
#df.columns = outpt.keys()
df = outpt.fetchall()


#SQL_statement2 = '''select * from stores where ac_nshopid in ('1800001404')'''


stores = ""
withs = ''
#input.join(str(x[0])+str(x[3]) for x in df)

for i,x in enumerate(df):
    if i == len(df)-1:
        stores = stores + "'" + str(x[0]) + "')))"
    elif i == 0:
        stores = stores + "('" + str(x[0]) + "',"
    else:
        stores = stores + "'" + str(x[0]) + "',"

    withs = withs + "when ac_nshopid = '" + str(x[0]) + "'  then  '" + str(x[4]) + "' \n"



print(stores)
print(withs)

SQL_statement2 = '''
 select ac_nshopid,nc_slot2,fa,slot2_n, 
 'UC;BUTOM_CORR;NO;;'||nc_periodid||';;'|| 
  ac_nshopid||';'||ac_cref||';'||ac_crefsuffix||';'||ac_dtgroup||';;;;'||slot2_n||';;;;;;;;;;;BUTOM_CORR'||';'||nc_hash_signature as state    
 from (select  
 nc_periodid,ac_nshopid,ac_cref,ac_crefsuffix,ac_dtgroup,nc_slot2,fa,nc_slot2*fa as slot2_n,nc_hash_signature 
 from (select  
 nc_periodid,ac_nshopid,ac_cref,ac_crefsuffix,ac_dtgroup,nc_slot2,nc_hash_signature, 
 case  
{withs}
end "FA"
  
 from vldrawdata_no.rawdata 
 where nc_periodid = {period} --change period 
 and ac_dtgroup = 'VOL_BUTOM' 
 and ac_nshopid in {shops}
'''


SQL_statement2 = SQL_statement2.format(withs=withs, shops=stores, period=period)
print(SQL_statement2)


outpt2 = con.execute(SQL_statement2)
df2 = pd.DataFrame(outpt2.fetchall())
df2.columns = outpt2.keys()

SQL_statement3 = '''
 select 'UC;BUTOM_corr;NO;;'||r.nc_periodid||';;'||r.ac_nshopid||';'||r.ac_cref||';'||r.ac_crefsuffix||';VOL_BUTOM;;;;'||r.nc_slot2*rs.ratio||';;;;;;;;;;;BUTOM_corr'||';'||r.nc_hash_signature as state from rawdata r, 
 (select c.ac_nshopid, c.nc_periodid as crrnt_perid, p.nc_periodid as prev_perid, c.nc_statvalue as crrnt_value,p.prev_value as prev_value, round((c.nc_statvalue/p.prev_value),2) as ratio from  
 (select nc_periodid,ac_nshopid,nc_statvalue from storestats 
 where nc_periodid = {period}--current period 
 and ac_processid = 'ESTIMATE' 
 and ac_dtgroup = 'VOLUMETRIC' 
 and ac_stattag = 'VAL_OUT') c,(select r.nc_periodid,r.ac_nshopid,r.ac_dtgroup,sum(r.nc_slot2) as prev_value from rawdata r, 
                               
                               (SELECT AC_NSHOPID,max(nc_periodid) as period FROM STOREMONITOR 
                                                    WHERE AC_MONITORTAG = '999999'  
                                                   AND AC_MONITORSTATUS = '3' 
                                                    and ac_dtgroup = 'VOLUMETRIC' 
                                                   and ac_nshopid in (SELECT AC_NSHOPID FROM STOREMONITOR 
                                                    WHERE NC_PERIODID = {period}  
                                                   AND AC_MONITORTAG = '999999'  
                                                   AND AC_MONITORSTATUS = '12') 
                                                   group by ac_nshopid) st 
                                                   where r.ac_nshopid= st.ac_nshopid and r.nc_periodid = st.period                                                   
                                                   and r.ac_dtgroup = 'VOLUMETRIC' 
 group by r.nc_periodid,r.ac_nshopid,r.ac_dtgroup) p 
 WHERE c.ac_nshopid =p.ac_nshopid) rs 
  
 where r.ac_nshopid = rs.ac_nshopid 
 and r.nc_periodid = rs.crrnt_perid 
 and r.nc_periodid = {period}
 and r.ac_dtgroup = 'VOL_BUTOM'
'''
SQL_statement3 = SQL_statement3.format(period=period)
print(SQL_statement3)

outpt3 = con.execute(SQL_statement3)
df3 = pd.DataFrame(outpt3.fetchall())
df3.columns = outpt3.keys()

con.close()
print(df2['state'].head())

df2['state']=df2['state'].astype(str)
df3['state']=df3['state'].astype(str)


#df['state'] = df['state'].apply(lambda x: x.replace('"', ''))
### test = df2.state.apply(lambda x: x.replace('"', ''))
#print(test)


state_xcopy=df2[['state']]
state_est=df3[['state']]
#print(type(state))
state_xcopy.to_csv(file_name,index=False,header=False, quoting=csv.QUOTE_NONE)
state_est.to_csv(file_name2,index=False,header=False, quoting=csv.QUOTE_NONE)





