from sqlalchemy import create_engine
import pandas as pd
import numpy as np
from sklepy import dniMiesiace, period_update
import os
import db_connect as db
import datetime

con = db.engine.connect()

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

country_id = 'no'

#sklepy do changes
shops = '''('1800001238',
'1800005722',
'1800005973',
'1800006191',
'1800007816',
'1800007931',
'1800009778'
)
'''

file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\storestable.txt'
#file_name = ''
file_name2 = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\Sirval_Changes_'+str(year)+str(week)+'.csv'
dir_name = os.path.dirname(file_name2)

# czesc do ladowania
if file_name != '':
    head = ['AC_NSHOPID' ,'AC_SHOPDESCRIPTION', 'AC_SHOPSTATUS', 'AC_DEFAULTXCODEGR', 'AC_COUNTRYID', 'AC_RETAILER', 'AC_LANGUAGEID', 'AC_SHOPTYPE', 'AC_AREA','NC_SURFACE','NC_ACV','NC_XF','NC_ACTIVEFLAG','NC_DUPITEMS_FLAG','NC_EANXCODE_FLAG','AC_ICELL','AC_CHANNELID','AC_IBD','NC_PERIODID','NC_DUMMY_FLAG','AC_EBD']
    stores = pd.read_csv(file_name, sep=';', header=None, names=head, dtype='str', encoding='utf8')

    stores["AC_COMMENT"] = 'NO'
    stores["DT_INSTIMESTAMP"] = np.nan
    stores["AC_INSUSER"] = np.nan
    stores["DT_UPDTIMESTAMP"] = np.nan
    stores["AC_UPDUSER"] = np.nan
    stores = stores[['AC_NSHOPID', 'AC_SHOPSTATUS', 'AC_DEFAULTXCODEGR', 'AC_COUNTRYID', 'AC_LANGUAGEID', 'NC_ACV', 'NC_ACTIVEFLAG', 'NC_DUPITEMS_FLAG', 'NC_EANXCODE_FLAG','AC_CHANNELID','NC_DUMMY_FLAG','AC_SHOPDESCRIPTION','AC_RETAILER','AC_SHOPTYPE','AC_AREA','NC_SURFACE','AC_IBD','NC_XF','AC_COMMENT','DT_INSTIMESTAMP','AC_INSUSER','DT_UPDTIMESTAMP','AC_UPDUSER','NC_PERIODID','AC_ICELL','AC_EBD']]
    name = dir_name+'\stores.txt'
    stores.to_csv(name, sep=';', header=False, index=False)

# czesc do changes
SQL_statement = '''
select * from stores where ac_nshopid in {shops}
'''

changes = pd.read_csv(file_name2, sep=';', dtype=str ,encoding='utf8')
print(changes.head())
#changes.NC_XF = pd.to_numeric(changes.NC_XF , downcast='float')
SQL_statement = SQL_statement.format(country=country_id, shops=shops)


outpt = con.execute(SQL_statement)
df = pd.DataFrame(outpt.fetchall())
df.columns = outpt.keys()

con.close()

#df.dt_instimestamp = df.dt_instimestamp.astype(float)
print(df.info())

changes_all = pd.merge(changes, df, left_on='AC_NSHOPID', right_on='ac_nshopid')
changes_all = changes_all[['AC_NSHOPID', 'AC_SHOPDESCRIPTION', 'AC_SHOPSTATUS', 'AC_DEFAULTXCODEGR', 'ac_countryid', 'AC_RETAILER', 'ac_languageid', 'AC_SHOPTYPE','AC_AREA','NC_SURFACE','AC_IBD','NC_ACV','NC_XF','ac_comment','nc_activeflag','ac_insuser','ac_upduser','NC_PERIODID','nc_dupitems_flag','nc_eanxcode_flag','AC_ICELL','AC_EBD','ac_channelid','NC_DUMMY_FLAG']]
#changes_all['dt_instimestamp'] = np.nan
#changes_all['dt_updtimestamp'] = np.nan

print(df[['dt_instimestamp', 'dt_updtimestamp']].head())

name2 = dir_name+'\changes_backup.txt'
name3 = dir_name+'\changes_load.txt'

df.to_csv(name2, sep=';', index=False)
changes_all.to_csv(name3, sep=';', index=False, header=False)
