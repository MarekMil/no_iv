import pandas as pd
import easygui as eg
import db_connect as db
import datetime
from sklepy import dniMiesiace

con = db.engine.connect()
#automatyczny update periodu
dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

#file = eg.fileopenbox(default='S:\\srvlload\\', filetypes='.csv', multiple=False)
days = pd.read_csv(r'\\Acn047oslfsv04\s\srvlload\COOPSHOP_DeliveryDays_'+str(year[-2:])+str(week)+'.csv', sep=';', dtype=str, encoding='utf8')

days['Selling'] = days[['day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7']].count(axis='columns')
days.shop = days.shop.astype(int)

print(days.info())
sample = pd.ExcelFile(r'\\acn047oslfsv05\statavd\Sample\Sample.xlsx').parse('Sample')

sample = sample[['MTNR', 'RTL', 'RTLNR', 'ActiveInSample', 'Butnavn2']]

sample = sample[(sample['RTL'] == 'C1') | (sample['RTL'] == 'C2') | (sample['RTL'] == 'C3') | (sample['RTL'] == 'C4') |
                (sample['RTL'] == 'C5') | (sample['RTL'] == 'C6') | (sample['RTL'] == 'C7')]


# print(sample.info())
days_sample = pd.merge(days, sample, left_on='shop', right_on='RTLNR', how='left')
# print(days_sample.head())
changes = pd.ExcelFile(r'\\acn047oslfsv05\statavd\Sample\ChangesInSampleShops.xlsx').parse('CHANGES')

final = pd.merge(days_sample, changes, left_on='MTNR', right_on='mtnr', how='left')

print(final.info())

final = final.sort_values(by=['Selling'])
print(final.tail(1))
print(type(final.tail(1)))
transp = final.tail(1)[['day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7']].transpose()
print(transp.columns[0].astype(int))

print(type(transp))
transp = transp.sort_values(transp.columns[0], ascending = True)
print(list(transp.index.values))
f=['MTNR']+list(transp.index.values) +['Selling','ActiveInSample', 'Butnavn2','dato', 'comm']
print(f)
final[f].to_excel(r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\COOPSHOP_DeliveryDays_'+str(year[-2:])+str(week)+'.xlsx','Sheet1', index=False)
