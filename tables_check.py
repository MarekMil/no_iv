import os
import datetime
import pandas as pd
import db_connect as db

def dates(past=0):
    dayz = 7
    now=datetime.datetime.now()-datetime.timedelta(days= past)
    month, day = now.month, now.day
    week = (datetime.datetime.now() - datetime.timedelta(days=dayz)).isocalendar()[1]
    year = (datetime.datetime.now()-datetime.timedelta(days=dayz+past)).year
    if len(str(day)) == 1:
        day = '0' + str(day)

    if len(str(month)) == 1:
        month = '0' + str(month)

    if len(str(week)) == 1:
        week = '0' + str(week)

    return {'year':str(year),'month':str(month),'day':str(day), 'week':str(week)}

dates_ = dates()
year, week = dates_['year'], dates_['week']
#year =''
#week = ''

ch = {6:-1,7:-2, 8:-3}

path = r'G:\Team Drives\NO BAU IV\IV_production\\'+year+'w'+week
#path = r'C:\Nielsen Pliki\SIRVAL DENMARK\DK BAU IV\Bulk Load and Update\\'

flagA, flagB, flagC, flagD, flagE, flag_missing = False, False, False, False, False, False

#docx = year + week +'_Lbatchstores_update.docx'

storestable = 'storestable.txt'
lbatchstores = 'Lbatchstores.txt'
storesdtgroups = 'Storedtgroups.txt'
storexcodegroup = 'storexcodegroup.txt'
changes = 'Change.txt'

enc = 'utf8'

stores_tbl=('AC_NSHOPID', 'AC_SHOPSTATUS', 'AC_DEFAULTXCODEGR', 'AC_COUNTRYID', 'AC_LANGUAGEID', 'NC_ACV',
            'NC_ACTIVEFLAG', 'NC_DUPITEMS_FLAG', 'NC_EANXCODE_FLAG', 'AC_CHANNELID', 'NC_DUMMY_FLAG',
            'AC_SHOPDESCRIPTION', 'AC_RETAILER', 'AC_SHOPTYPE', 'AC_AREA', 'NC_SURFACE', 'AC_IBD', 'NC_XF',
            'NC_PERIODID', 'AC_ICELL', 'AC_EBD')

lbatch_tbl=('ac_lbatchid', 'ac_cshopid','ac_nshopid', 'nc_cshopidreq', 'nc_activeflag', 'nc_periodid')

dtgroup_tbl = ('ac_nshopid', 'ac_dtgroup', 'nc_DTGROUPREQ', 'nc_activeflag', 'nc_periodkeeponbuffer')

xcodegr_tbl = ('ac_nshopid', 'ac_xcodegr', 'nc_xcodegrseq', 'nc_peractivefrom', 'nc_peractiveto')

sirval_changes_tbl = ('AC_NSHOPID', 'AC_SHOPSTATUS', 'AC_DEFAULTXCODEGR', 'NC_ACV', 'NC_DUMMY_FLAG',
                      'AC_SHOPDESCRIPTION', 'AC_RETAILER', 'AC_SHOPTYPE', 'AC_AREA', 'NC_SURFACE',
                      'AC_IBD', 'NC_XF', 'NC_PERIODID', 'AC_ICELL', 'AC_EBD')

df_stores = pd.DataFrame()
df_lbatch = pd.DataFrame()
df_dtgroup = pd.DataFrame()
df_xcodegr = pd.DataFrame()
df_changes = pd.DataFrame()
missing_lines = []

os.chdir(path)

con=db.engine.connect()

#zgoda='y'
#zgoda = input('Załadowane(y/n)?: ')


if os.path.isfile(storestable):
    with open(storestable, 'r', encoding=enc) as stores:
        for line in stores:
            values = line.split(';')
            values[-1] = values[-1][:-1]
            if len(values[17]) > 6:
                pre, post = values[17].split('.')
                if len(post) > 5:
                    for n in reversed(range(len(post))):
                        if post.startswith('0' * n):
                            save = '0' * n
                            break
                    post = str(round(int(post), ch[len(post)]))
                values[17] = pre + '.' + save + post[:5-len(save)]
            SQL_stores = '''select {} from stores where ac_nshopid ={}'''\
                .format(', '.join(stores_tbl), '\'' + str(values[0]) + '\'')
            stores_outpt = con.execute(SQL_stores).fetchall()
            if len(stores_outpt) == 0:
                missing_lines.append('STORES: ' + ';'.join(values)+'\n')
                flag_missing = True

            else:
                for i in range(len(values)):
                    if str(values[i]) != str(stores_outpt[0][i]):
                        if len(df_stores) == 0:
                            df_stores = pd.DataFrame({'Shop': [values[0]], 'Column': [stores_tbl[i]], 'File': [values[i]],
                                                      'Sirval': [stores_outpt[0][i]]})
                        else:
                            df = pd.DataFrame({'Shop': [values[0]], 'Column': [stores_tbl[i]], 'File': [values[i]],
                                               'Sirval': [stores_outpt[0][i]]})
                            df_stores = pd.merge(df_stores, df, how='outer', on=['Shop', 'Column', 'File', 'Sirval'])
                        flagA = True
    stores.close()

if os.path.isfile(changes):
    with open(changes, 'r', encoding=enc) as changes:
        for line5 in changes:
            values5 = line5.split(';')
            print(values5)
            values5[-1] = values5[-1][:-1]
            if len(values5[11]) > 6:
                pre, post = values5[11].split('.')
                if len(post) > 5:
                    for n in reversed(range(len(post))):
                        if post.startswith('0' * n):
                            save = '0' * n
                            break
                    post = str(round(int(post), ch[len(post)]))
                values5[11] = pre + '.' + save + post[:5-len(save)]
            SQL_changes = '''select {} from stores where ac_nshopid ={}''' \
                .format(', '.join(sirval_changes_tbl), '\'' + str(values5[0]) + '\'')
            changes_outpt = con.execute(SQL_changes).fetchall()
            if len(changes_outpt) == 0:
                missing_lines.append('STORES: ' + ';'.join(values5)+'\n')
                flag_missing = True
            else:
                for i in range(len(values5)):
                    if str(values5[i]) != str(changes_outpt[0][i]):
                        if len(df_changes) == 0:
                            df_changes = pd.DataFrame(
                                {'Shop': [values5[0]], 'Column': [sirval_changes_tbl[i]], 'File': [values5[i]],
                                 'Sirval': [changes_outpt[0][i]]})
                        else:
                            df = pd.DataFrame(
                                {'Shop': [values5[0]], 'Column': [sirval_changes_tbl[i]], 'File': [values5[i]],
                                 'Sirval': [changes_outpt[0][i]]})
                            df_changes = pd.merge(df_changes, df, how='outer',
                                                  on=['Shop', 'Column', 'File', 'Sirval'])
                        flagE = True
        changes.close()

if os.path.isfile(lbatchstores):
    with open(lbatchstores, 'r', encoding=enc) as lbatch:
        for line2 in lbatch:
            values2 = line2.split(';')
            values2[-1] = values2[-1][:-1]
            SQL_lbatch = ''' select {} from lbatchstores where ac_nshopid ={} and ac_lbatchid ={}''' \
                .format(', '.join(lbatch_tbl), '\'' + str(values2[2]) + '\'', '\'' + str(values2[0] + '\''))
            lbatch_outpt = con.execute(SQL_lbatch).fetchall()
            if len(lbatch_outpt) == 0:
                missing_lines.append('LBATCHSTORES: ' + ';'.join(values2)+'\n')
                flag_missing = True
            else:
                for i in range(len(values2)):
                    if str(values2[i]) != str(lbatch_outpt[0][i]):
                        if len(df_lbatch) == 0:
                            df_lbatch = pd.DataFrame(
                                {'Shop': [values2[2]], 'Column': [lbatch_tbl[i]], 'File': [values2[i]],
                                 'Sirval': [lbatch_outpt[0][i]]})
                        else:
                            df = pd.DataFrame({'Shop': [values2[2]], 'Column': [lbatch_tbl[i]], 'File': [values2[i]],
                                               'Sirval': [lbatch_outpt[0][i]]})
                            df_lbatch = pd.merge(df_lbatch, df, how='outer', on=['Shop', 'Column', 'File', 'Sirval'])
                        flagB = True
    lbatch.close()


if os.path.isfile(storesdtgroups):
    with open(storesdtgroups, 'r', encoding=enc) as dtgroup:
        for line3 in dtgroup:
            values3 = line3.split(';')
            values3[-1] = values3[-1][:-1]
            SQL_dtgroup = ''' select {} from storedtgroups where ac_nshopid ={} and ac_dtgroup ={}''' \
                .format(', '.join(dtgroup_tbl),'\'' + str(values3[0]) + '\'', '\'' + str(values3[1] + '\''))
            dtgroup_outpt = con.execute(SQL_dtgroup).fetchall()
            if len(dtgroup_outpt) == 0:
                missing_lines.append('DTGROUP: ' + ';'.join(values3)+'\n')
                flag_missing = True
            else:
                for i in range(len(values3)):
                    if str(values3[i]) != str(dtgroup_outpt[0][i]):
                        if len(df_dtgroup) == 0:
                            df_dtgroup = pd.DataFrame(
                                {'Shop': [values3[0]], 'Column': [dtgroup_tbl[i]], 'File': [values3[i]],
                                 'Sirval': [dtgroup_outpt[0][i]]})
                        else:
                            df = pd.DataFrame({'Shop': [values3[0]], 'Column': [dtgroup_tbl[i]], 'File': [values3[i]],
                                               'Sirval': [dtgroup_outpt[0][i]]})
                            df_dtgroup = pd.merge(df_dtgroup, df, how='outer', on=['Shop', 'Column', 'File', 'Sirval'])
                        flagC = True
        dtgroup.close()

if os.path.isfile(storexcodegroup):
    with open(storexcodegroup, 'r', encoding=enc) as xcodegroup:
        for line4 in xcodegroup:
            values4 = line4.split(';')
            values4[-1] = values4[-1][:-1]
            SQL_xcodegr = ''' select {} from storexcodegroups where ac_nshopid ={} and nc_xcodegrseq ={}''' \
                .format(', '.join(xcodegr_tbl),'\'' + str(values4[0]) + '\'', '\'' + str(values4[2] + '\''))
            xcodegr_outpt = con.execute(SQL_xcodegr).fetchall()
            if len(xcodegr_outpt) == 0:
                missing_lines.append('XCODEGROUPS: ' + ';'.join(values4)+'\n')
                flag_missing = True
            else:
                for i in range(len(values4)):
                    if str(values4[i]) != str(xcodegr_outpt[0][i]):
                        if len(df_xcodegr) == 0:
                            df_xcodegr = pd.DataFrame(
                                {'Shop': [values4[0]], 'Column': [xcodegr_tbl[i]], 'File': [values4[i]],
                                 'Sirval': [xcodegr_outpt[0][i]]})
                        else:
                            df = pd.DataFrame({'Shop': [values4[0]], 'Column': [xcodegr_tbl[i]], 'File': [values4[i]],
                                               'Sirval': [xcodegr_outpt[0][i]]})
                            df_xcodegr = pd.merge(df_xcodegr, df, how='outer', on=['Shop', 'Column', 'File', 'Sirval'])
                        flagD = True
    xcodegroup.close()
#

#
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    if any([flagA, flagB, flagC, flagD, flagE, flag_missing]):
        print('\n**************************************************************************')
        if flagA and flagE:
            print('                  STORESTABLE\n')
            print(df_stores)
            print(df_changes)
            print()
        elif flagA and not flagE:
            print('                  STORESTABLE\n')
            print(df_stores)
            print()
        elif not flagA and flagE:
            print('                  STORESTABLE\n')
            print(df_changes)
            print()
        if flagB:
            print('                  LBATCHSTORES\n')
            print(df_lbatch)
            print()
        if flagC:
            print('                  DTGROUP\n')
            print(df_dtgroup)
            print()
        if flagD:
            print('                  XCODEGR\n')
            print(df_xcodegr)
            print()
        if flag_missing:
            print('                  MISSING LINES IN SIRVAL\n')
            for line in missing_lines:
                print(line)
            print()
        print('**************************************************************************')
    else:
        print('''\n**************************************************
\(^ _ ^)/       Everything is fine!      \(^ _ ^)/
**************************************************''')
