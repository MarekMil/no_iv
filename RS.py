import pandas as pd
import string
import db_connect as db
import os
import shutil
import glob
from sklepy import dniMiesiace, period_update

def vatRS(period_change = 0):

    dateDct = dniMiesiace(period_change*7)
    year, week = dateDct['year'], dateDct['week']

    name_ra1 = r'\\Acn047oslfsv04\s\srvlload\RS'+str(year[-2:])+str(week)+'.RA1'
    name_oms = r'\\Acn047oslfsv04\s\RAWINPUT\rsh\oms'+str(year[-2:])+str(week)+'0.000'

    ra1 = pd.read_csv(name_ra1, sep=';', dtype=str, encoding='ansi', header=None)
    ra1.columns = ['row']
    ra1[1] = ra1.apply(lambda row: row[0][3:12].strip(), axis=1)
    ra1['code'] = ra1.apply(lambda row: row[0][12:26].strip(), axis=1)

    def code0(origin, sale):
        return origin[0:59] + str(int(float(sale)/1.25)) + ' ' * (9-len(sale)) + origin[68:]


    oms = pd.read_csv(name_oms, sep=';', dtype=str, encoding='latin-1', header=None)
    oms[3] = oms.apply(lambda row: ((13-len(row[3]))*'0') + row[3], axis=1)
    merged = pd.merge(ra1, oms,  how='inner', left_on=[1, 'code'], right_on=[0, 3])
    print(ra1.head())
    print(oms.head())
    print(merged.head())

    #merged[12] = merged[12].astype(float)

    def vat_rate(origin,sale):
        return origin[0:58] + str(round(float(sale.replace(',', '.'))*100)) + ' ' * (10-len(str(round(float(sale.replace(',', '.'))*100)))) + origin[68:]

    merged['output'] = merged.apply(lambda row: vat_rate(row['row'], row[12]), axis=1)

    merged[['output']].to_csv(r'\\Acn047oslfsv04\s\srvlload\RS'+str(year[-2:])+str(week)+'.RA1', index=False, header=False, quoting=3, sep=';', encoding='ansi')

    print('Przerobiłem RS.RA1')

    os.chdir(r'\\Acn047oslfsv04\s\srvlload')
    # os.chdir(r'C:\Users\mima8005\Desktop\Test')
    for file in glob.glob("RS" + str(year[-2:])+str(week) + ".RA1"):
        shutil.copy2(r'\\Acn047oslfsv04\s\srvlload\\' + str(file),
                     r'\\Acn047oslfsv04\s\srvlload\\' + "NV" + str(year[-2:])+str(week) + ".RA1")
        print('Skopowiałem: ' + "NV" + str(year[-2:])+str(week) + ".RA1")
        shutil.copy2(r'\\Acn047oslfsv04\s\srvlload\\' + str(file),
                     r'\\Acn047oslfsv04\s\srvlload\\' + "SE" + str(year[-2:])+str(week) + ".RA1")
        print('Skopowiałem: ' + "SE" + str(year[-2:])+str(week) + ".RA1")

