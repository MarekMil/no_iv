import urllib.request, json
import pandas as pd
import datetime
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
from bs4 import BeautifulSoup
import requests

#dictionary containing spatial data for specific countries
sites = {'se':['https://spatial.virtualearth.net/REST/v1/data/b340d487953044ba8e2b20406ce3fcc6/Filialdaten-SE/Filialdaten-SE?spatialFilter=nearby(59.3226729,18.0344329,999)&$filter=Adresstyp%20Eq%201&%24select=EntityID%2CLatitude%2CLongitude%2CAddressLine%2CPostalCode%2CLocality%2CBilanzgesellschaft%2COpeningTimes%2CINFOICON1%2CINFOICON2%2CINFOICON3%2CINFOICON4%2CINFOICON5%2CAR%2CNF%2CAdresstyp%2COverlayOperatorData&$top=20000&$format=json&$skip=0&key=AiHIKQCACRaaOyOJQjGEGl5uxp7KOTXwae435wJqW3jBo_HLpRWmOVrhOI-eI-Rj&Jsonp=displayResultStores',
               'https://spatial.virtualearth.net/REST/v1/data/b340d487953044ba8e2b20406ce3fcc6/Filialdaten-SE/Filialdaten-SE?spatialFilter=nearby(64.6981657,19.1257829,999)&$filter=Adresstyp%20Eq%201&%24select=EntityID%2CLatitude%2CLongitude%2CAddressLine%2CPostalCode%2CLocality%2CBilanzgesellschaft%2COpeningTimes%2CINFOICON1%2CINFOICON2%2CINFOICON3%2CINFOICON4%2CINFOICON5%2CAR%2CNF%2CAdresstyp%2COverlayOperatorData&$top=20000&$format=json&$skip=0&key=AiHIKQCACRaaOyOJQjGEGl5uxp7KOTXwae435wJqW3jBo_HLpRWmOVrhOI-eI-Rj&Jsonp=displayResultStores'],
         'dk':['https://spatial.virtualearth.net/REST/v1/data/9ca2963cb5f44aa3b4c241fed29895f8/Filialdaten-DK/Filialdaten-DK?spatialFilter=nearby(55.393,10.3859,999)&$filter=Adresstyp%20Eq%201&%24select=EntityID%2CLatitude%2CLongitude%2CAddressLine%2CPostalCode%2CLocality%2CBilanzgesellschaft%2COpeningTimes%2CINFOICON1%2CINFOICON2%2CINFOICON3%2CINFOICON4%2CINFOICON5%2CAR%2CNF%2CAdresstyp%2COverlayOperatorData&$top=20000&$format=json&$skip=0&key=AsaaAZuUgeIzOb829GUz0a2yjzX0Xw1-OTmjH_27CS5ilYr5v9ylNxg4rQSRhh8Z&Jsonp=displayResultStores',]}
#columns to pop
pops = ('__metadata', 'Latitude', 'Longitude', 'INFOICON1', 'INFOICON2', 'INFOICON3', 'INFOICON4','INFOICON5',
        'Bilanzgesellschaft', 'AR', 'NF', 'Adresstyp', 'OverlayOperatorData')
#backup path to save generated files
backup_path = r'G:\Team Drives\NO BAU IV\IV_production\SCRAPNET_STORES_DK&SE\\'

def dayz(past=0):
    '''translates current week to week week earlier, also adding '0' to 1 digital numbers
    - dayz - nielsen calendar takes 7 days prior current day
    - past - base value 0 - by providing integer allows to overwrite base setup by a number of provide days,
        for example: past = , takes date 10 before current (dayz=7 + past=3)
    - return - dictionary, with string values of year and week'''
    dayz = 7
    #recalculating week and year to nielsen's
    week = (datetime.datetime.now() - datetime.timedelta(days=dayz)).isocalendar()[1]
    year = (datetime.datetime.now()-datetime.timedelta(days=dayz+past)).year
    #adding 0 to one-digital week
    if len(str(week)) == 1:
        week = '0' + str(week)
    return {'year':str(year), 'week':str(week)}

def send_mail(date=dayz()):
    '''after providing NIELSEN date (date) sends mail with files included on hardcoded paths
    - gmail_user - user sending mail in form of string
    - to_addr - addresses you want to send mail to, in form of list of strings
    - gmail_password - password to gmail of sender, generated according to instruction:
    https://support.google.com/accounts/answer/185833?hl=en
    - returns - nothing'''

    #assigning current nielsen week and year
    week, year = date['week'], date['year']
    gmail_user = 'marek.milcarz@nielsen.com'
    to_addr = ['nordicstatisticaloperations@nielsen.com', 'stat.se@nielsen.com', 'msci.denmark@nielsen.com ']
    #to_addr = ['marek.milcarz@nielsen.com']
    gmail_password = 'knkouhzxasxywufh'
    #changes working directory to backup folder for current week
    os.chdir(backup_path + year + 'W' + week + '\\')
    #creates list of files currently present on path
    filenames = [os.path.join(f) for f in os.listdir(backup_path + year + 'W' + week + '\\')]
    #creating multipart mail body
    msg = MIMEMultipart()
    msg['From'] = gmail_user
    msg['To'] = ", ".join(to_addr)
    msg['Subject'] = 'STORES ' + year +'W'+week
    body = '''
    Hi,

    Please find attached LIDL and ALDI stores for week {week}.

    Regards.'''.format(week=str(week))
    msg.attach(MIMEText(body, 'plain'))
    #attaching multiple files to mail
    for file in filenames:
        p = MIMEBase('application', 'octet-stream')
        p.set_payload((open(file,'rb')).read())
        encoders.encode_base64(p)
        p.add_header('Content-Disposition', 'attachment; filename = %s' % file)
        msg.attach(p)
    #connecting to gmail
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.ehlo()
    server.login(gmail_user, gmail_password)
    #translating message of mail to string
    text = msg.as_string()
    #sending mail
    server.sendmail(gmail_user, to_addr, text)
    server.quit()

def dumping_to_dic(country, date=dayz()):
    whole = pd.DataFrame()
    for point in sites[country]:
        '''accessing informations from site(json data) and storing them to dictionary
        - country - string - shortcut of country for which data needs to accessed, either 'dk' or 'se'
        - date - nielsen date acquired from days'''
        skip = '''displayResultStores'''#text to skip of query result
        response = urllib.request.urlopen(point)#reading http
        the_page = response.read() # reading from site
        the_page = the_page[len(skip)+1:-1] # skipping some rubbish
        data = json.loads(the_page)#load json data to dictionary
        raw = data['d']['results']#simplifying dict

        '''cleaning data from rubbish and saving it to dataframe format
        - raw - dictionary - data acquired from site, passed by dumping_to_dic
        - country - passed from dumping_to_dic'''
        for item in raw:
            for single_pop in pops:
                item.pop(single_pop)#pops columns unnecessary to keep
            #cleaninc leftovers from html
            item['OpeningTimes'] = item['OpeningTimes'].replace('<b>', '')
            item['OpeningTimes'] = item['OpeningTimes'].replace('</b>', '')
            try:
                item['OpeningTimes'] = item['OpeningTimes'].replace('<br>', '')
            except:
                pass
            try:
                item['OpeningTimes'] = item['OpeningTimes'].replace('</font>', '')
            except:
                pass
            try:
                item['OpeningTimes'] = item['OpeningTimes'].replace('<font color=#D70000>', '')
            except:
                pass
        df = pd.DataFrame.from_dict(raw)#reading dictionary to dataframe
        df = df[['EntityID', 'AddressLine', 'PostalCode', 'Locality', 'OpeningTimes']]#rearranging columns
        whole = whole.append(df, ignore_index=True)

    '''creating directory for backup and saving data to file
    - dataframe - dataframe - data after cleaning, passed from cleaning
    - country - passed from cleaning
    - date - nielsen date acquired from days'''
    whole = whole.drop_duplicates()
    whole = whole.reset_index(drop=True)
    whole = whole.sort_values(by=['EntityID'])
    week, year = date['week'], date['year']
    #checking if directory for current week exists, if it doesnt then it makes one
    if not os.path.exists(backup_path+year+'W'+week+'\\'):
        os.mkdir(backup_path+year+'W'+week+'\\')
    os.chdir(backup_path+year+'W'+week+'\\')#changes working directory to backup folder for current week
    #dataframe.to_excel('LIDL_'+country.upper()+'.xlsx', index=False)#saving data to excel
    whole.to_csv(f'LIDL_{country.upper()}.txt', sep=';', index=False)#saving data to flat file format


#keys to look into the code of site
span_items = ('streetAddress', 'postalCode', 'addressLocality')
span_cont = ('Mo-Fr', 'Lør', 'Søn')

def aldi(date=dayz()):

    week, year = date['week'], date['year']#assigning current nielsen week and year
    os.chdir(backup_path + year + 'W' + week + '\\')#changes working directory to backup folder for current week
    source = requests.get('https://www.aldi.dk/find-butik.html').text#reading site to html
    soup = BeautifulSoup(source, 'lxml')#translating site to string-like format
    '''searching for all elements of code using as key tag:'a' and its class="link link--standard"
        those elements contain part of link calling subpage with list of 'subregions' for specific region'''
    regions = soup.find_all('a', class_="link link--standard")
    regions = regions[1:]
    whole =[]
    for region in regions:#creating loop on all regions
        '''accessing and reading data for specific subpage of subregion'''
        href = region['href']
        source_region = requests.get('https://www.aldi.dk'+href).text
        dressing = BeautifulSoup(source_region, 'lxml')
        '''searching for all elements of code using as key tag:'a' and its class="link link--standard"
                those elements contain part of link calling subpage with data of shops'''
        scripts = dressing.find_all('a', class_='link link--standard')
        stores=[]
        for script in scripts:#creating loop on all subregions
            '''accessing and reading data for specific subpage of store'''
            print(script)
            href = script['href']
            source_store = requests.get('https://www.aldi.dk'+href).text
            appetizer = BeautifulSoup(source_store, 'lxml')
            for i in range(len(appetizer.find_all('div', class_="mod-stores__overview-address"))):#creating loop on all stores
                values = []
                for item in span_items:#creating loop on data keys for store
                    contents = appetizer.find_all('span', itemprop=item)#searching adequate tag key
                    content = contents[i].text#acquiring text of adequate tag
                    values.append(content)#storing temporarily data
                times = appetizer.find_all('time', datetime=True, itemprop='openingHours')#searching adequate tag key
                for i in range(len(span_cont)):
                    time_ = times[i].text#acquiring text of adequate tag
                    #clering some rubbish
                    time_ = time_.strip('\t')
                    time_ = time_.strip('\n')
                    time_ = time_.strip()
                    values.append(time_)#storing temporarily data
                stores.append(values)#storing temporarily data per store
            data = pd.DataFrame(stores, columns=['streetAddress', 'postalCode', 'addressLocality', 'Mo-Fr', 'Lør', 'Søn'])#creating temporary dataframe for storing data for store
        whole.append(data)#storing data per region

    data_aldi = pd.DataFrame(pd.concat(whole, sort=True, axis=0))#storing data for whole aldi
    data_aldi = data_aldi[['streetAddress', 'postalCode', 'addressLocality', 'Mo-Fr', 'Lør', 'Søn']]#setting proper columns' names
    data_aldi.to_csv(f'ALDI_DK.txt', sep= ';', index=False)#saving data to flat file format

#calling functions to run
def run():
    dumping_to_dic('dk')
    dumping_to_dic('se')
    aldi()
    send_mail()

run()
