import pandas as pd
from sklepy import dniMiesiace

def sort(period_change=0):

    dateDct = dniMiesiace(period_change * 7)
    year, week = dateDct['year'], dateDct['week']


    file = r'\\Acn047oslfsv04\s\srvlload\LY'+str(year[-2:])+str(week)+'.RA1'
    #file = r'C:\Users\mima8005\Desktop\NO IV\2018w25\LY1825.RA1'
    data = pd.read_csv(file, header=None, skipinitialspace=True)

    data[1] = data.apply(lambda row: row[0][8:13], axis=1)
    data[2] = data.apply(lambda row: row[0][13:28], axis=1)
    data.sort_values([1, 2], ascending=[True, True], inplace=True)
    #data['first_name'] = data.apply(lambda row: row[0][0:3], axis=1)
    print(data.head())

    #costam.to_csv('fhf.txt', quoting=csv.QUOTE_NONE)
    #print(df.head())

    data[0].to_csv(r'\\Acn047oslfsv04\s\srvlload\LY'+str(year[-2:])+str(week)+'.RA1', header=None, index=None)
    #data[0].to_csv(r'C:\Users\mima8005\Desktop\NO IV\2018w25\LY1825_sorted.RA1', header=None, index=None)

    print('Skończylem sortować')
