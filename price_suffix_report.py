import db_connect as db
from openpyxl import load_workbook
import glob
import pandas as pd
from sklepy import dniMiesiace, period_update


con = db.engine.connect()
#automatyczny update periodu
period = period_update()

country_id = 'no'

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

SQL_statement = '''
select r.nc_periodid,ac_shoptype,r.ac_nshopid,ac_retailer,r.ac_cref,ac_crefsuffix,ac_crefdescription, 
ac_xcodegr,nc_conv,nc_slot1,nc_slot2, nc_slot2/nc_slot1 ,(nc_slot2/nc_slot1)/nc_conv,nc_price_mean,r.f_nan_key,ac_nandescrshort,ac_moduledescription,r.nc_hash_signature from rawdata r, 
descriptions d,vldimdb_no.nans n,vldimdb_no.modules m,stores s,ibenchmarks i
where r.nc_periodid = {period}
and ac_crefsuffix like 'P%' 
and r.ac_nshopid=s.ac_nshopid 
and r.ac_cref=d.ac_cref 
and r.f_nan_key=ac_itemkey 
and ac_itemtype='_NAN_' 
and ac_shoptype=substr(ac_storegroup,4) 
and r.f_nan_key=n.f_nan_key 
and n.nc_moduleid=m.nc_moduleid
and m.nc_moduleid in (SELECT NC_MODULEID FROM MODULES WHERE (NC_SUPERGROUPID IN (1,16,23) OR NC_PRODGROUPID = 8235))  
and r.nc_hash_signature=d.nc_hash_signature
'''
SQL_statement = SQL_statement.format(period=str(period))

outpt = con.execute(SQL_statement)
price_new = pd.DataFrame(outpt.fetchall())
price_new.columns = outpt.keys()

files = glob.glob(r'G:\Team Drives\NO BAU IV\IV_production\Price_suffixes\Price*')

history=[]
i=0
for f in files:

    wb = load_workbook(filename=f, data_only=True)
    ws = wb['Export Worksheet']

    data = ws.values
    excel = list(data)

    names = excel[0]
    df = pd.DataFrame(excel[1:], columns=names)

    x = [ws.cell(i+1, 1).fill.start_color.index for i, row in enumerate(ws.rows)]

    df['color'] = x[1:]
    df = df.dropna(axis='columns')
    df = df[df['color'] != '00000000']
    df = df[['AC_CREF', 'AC_CREFSUFFIX', 'AC_RETAILER','NC_PERIODID']]
    if i>0:
        result = pd.merge(result, df, on=['AC_CREF', 'AC_CREFSUFFIX', 'AC_RETAILER'], how='outer')
    else:
        result = df
    i = i+1


final = pd.merge(result, price_new, left_on=['AC_CREF', 'AC_CREFSUFFIX', 'AC_RETAILER'], right_on=['ac_cref', 'ac_crefsuffix', 'ac_retailer'], how='right')

final.drop_duplicates(subset=None, keep='first', inplace=True)
final.to_excel(r'H:\Team Drives\NO BAU IV\IV_production\Price_suffixes_'+str(week)+'.xlsx', 'QUESTIONS', index=False)
