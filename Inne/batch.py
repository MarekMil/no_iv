import pandas as pd
import numpy as np

file = r'C:\Users\olwo7001\Desktop\SIRVAL_Denmark\Causal tagging\PCANODISPLAY.2018-WK-002.detail'
head = ['AC_PERIODLABEL','AC_LBATCHID','AC_CSHOPID','AC_CREF','k1','k2','AC_CREFDESCRIPTION','k3','k4','k5','k6','k7','AC_CREFSTATUS','AC_DTGROUP','k8','NC_SLOT1','NC_SLOT2','NC_SLOT3','NC_SLOT4','NC_SLOT5','NC_SLOT6','NC_SLOT7','NC_SLOT8','NC_SLOT9','NC_SLOT10','AC_CSLOT1','AC_CSLOT2']
#dtype={'A': np.int64, 'B': np.float64}
data = pd.read_csv(file, sep='\t', header=None, names=head, dtype='str' ,encoding='cp865')

data.AC_CREF = data.AC_CREF.astype(str)
data.info()
data['length'] = data.AC_CREF.apply(lambda x: len(x))
#print(data[data['AC_CREF'].isnull()])

#print(test.unique())
sumup = data.groupby('length')['AC_CREF'].count()
test=pd.pivot_table(data, values='AC_CREF', index='length', aggfunc='count')

print(test)
# periods=np.linspace(1976,1977,2)
# print(periods)
# print(type(periods))
