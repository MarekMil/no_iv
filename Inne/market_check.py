import pandas as pd
import banner_plot as bp
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import db_connect as db

con = db.engine.connect()


country_id = 'no'
period = 1995
# period = input('Production period: ')
transistion_period = '1917'


shops = '''(
'1800006137',
'1800008478',
'1800008513',
'1800008523',
'1800009919'
)
'''

file_name = r'C:\Users\olwo7001\Desktop\SIRVAL_Norway\2018_13_1995\volumetric\ShopsToChecks_{period}.pdf'.format(period=period)
pdf = matplotlib.backends.backend_pdf.PdfPages(file_name)

SQL_statement = '''
select ( CASE WHEN cy.ac_periodlabel is NULL THEN  ly.ac_periodlabel ELSE cy.ac_periodlabel END) as ac_periodlabel,
( CASE WHEN cy.periodid is NULL THEN  ly.periodid ELSE cy.periodid END) as periodid,
( CASE WHEN cy.ac_nshopid is NULL THEN  ly.ac_nshopid ELSE cy.ac_nshopid END) as ac_nshopid,
cy.retailer, cy.items as n_records, cy.value as n_value, ly.items  as old_records, ly.value as old_value from
(select distinct ac_periodlabel, m.nc_periodid as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='TOT_UNITS' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='TOT_VALUES' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in  {shops}
and ac_dtgroup='VOLUMETRIC' and (p.nc_periodid between {period} - 52 and {period})  and p.nc_periodid>{tr_period} group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
UNION
select distinct ac_periodlabel, m.nc_periodid as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='SUM_SLOT1' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='SUM_SLOT2' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in {shops}
 and ac_dtgroup='VOL_RCC' and (p.nc_periodid between {period} - 52 and {period})  and p.nc_periodid<={tr_period}  group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
) cy
FULL OUTER JOIN
(select distinct ac_periodlabel, m.nc_periodid+52 as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='TOT_UNITS' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='TOT_VALUES' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in  {shops}
 and ac_dtgroup='VOLUMETRIC' and (p.nc_periodid between {period} - 104 and {period} - 52)  and p.nc_periodid>{tr_period} group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
UNION
select distinct ac_periodlabel, m.nc_periodid+52 as periodid, ac_retailer as retailer, m.ac_NSHOPID, 
       max(case when ac_stattag='NREC_LOADED' then nc_statvalue end) as items,
       max(case when ac_stattag='SUM_SLOT1' then nc_statvalue end) as SUMUNITS,
       max(case when ac_stattag='SUM_SLOT2' then nc_statvalue end) as value
from vldprocess_{country}.storestats m,
     vldinstr_{country}.stores s,
     periods p 
where m.ac_NSHOPID=s.ac_NSHOPID and m.nc_periodid=p.nc_periodid and ac_processid in ('BL') and m.ac_NSHOPID in {shops}
 and ac_dtgroup='VOL_RCC' and (p.nc_periodid between {period} - 104 and {period} - 52) and p.nc_periodid<={tr_period}  group by ac_periodlabel,m.nc_periodid,ac_retailer,m.ac_NSHOPID
) ly
ON cy.periodid=ly.periodid and cy.ac_nshopid = ly.ac_nshopid
ORDER BY ac_nshopid, periodid'''

SQL_statement = SQL_statement.format(country=country_id, period=period, tr_period=transistion_period, shops=shops)
print(SQL_statement)

outpt = con.execute(SQL_statement)
df = pd.DataFrame(outpt.fetchall())
df.columns = outpt.keys()
print(df.head())
con.close()
df.n_value = df.n_value.astype(float)
df.old_value = df.old_value.astype(float)
df.ac_periodlabel = df.ac_periodlabel.astype("category")

#filtr = np.logical_and(df['n_records'].notnull(), df['old_records'].notnull())
#df[['n_value', 'n_records', 'old_value', 'old_records']] = df[['n_value', 'n_records', 'old_value', 'old_records']].interpolate()
print(df)
'''
df['old_records'] = df['old_records'].interpolate()
df['old_value'] = df['old_value'].interpolate()
df['n_records'] = df['n_records'].interpolate()
df['n_value'] = df['n_value'].interpolate()
'''
#df.retailer = df.retailer.astype(str)
#df['retailer'] = df['retailer'].apply(lambda x: x[3:])

#filtr = df['n_records'].notnull()
#df = df[filtr]
#print(df)

for store in df['ac_nshopid'].unique():
    print(store)
    df1 = df[df.ac_nshopid == store]
    df1[['n_value', 'n_records', 'old_value', 'old_records']] = df1[['n_value', 'n_records', 'old_value', 'old_records']].interpolate()
    #print(df1.head())
    #print(df1.info())
    fig1, ax1 = plt.subplots()
    shopt, typ = bp.banner_p(country_id, str(df1.retailer.unique()[0]))
    #shopt, typ='',''
    title = 'Banner={retailer}  AC_NSHOPID={shop} \n index = {index}  type = {typ} \n dashed lines = last year'.format(retailer=str(df1.retailer.unique()[0]), shop=store, index=shopt, typ=typ)
    plt.title(title, fontsize=7)
    #plt.suptitle('\n dashed lines = last year\n', fontsize=10)
    plt.xticks(df1['periodid'], fontsize=5, rotation=90)
    plt.yticks(fontsize=6)
    plt.grid(b=True, which='both', linewidth=0.2)
    color = 'tab:red'
    ax1.set_xlabel('Weeks')
    ax1.set_ylabel('Sales value', color=color)
    ax1.plot(df1['periodid'], df1['n_value'], color='red', linewidth=0.75, marker='.', markersize=1)
    ax1.plot(df1['periodid'], df1['old_value'], color='red', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.set_xticklabels(df1['ac_periodlabel'])
    ax1.set_ylim(ymin=0)

    ax2 = ax1.twinx()
    plt.yticks(fontsize=6)
    color = 'tab:green'
    ax2.set_ylabel('Records', color=color)  # we already handled the x-label with ax1
    ax2.plot(df1['periodid'], df1['n_records'], color='green', linewidth=0.75, marker='.', markersize=1)
    ax2.plot(df1['periodid'], df1['old_records'], color='green', linestyle='dashed', linewidth=0.75, marker='.', markersize=1)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_ylim(ymin=0)

    fig1.tight_layout()  # otherwise the right y-label is slightly clipped


    pdf.savefig(fig1)


pdf.close()

'''
print('aksssssssss')
print(np.size(df.ac_periodlabel))
print('aksssssssss')
print(np.size(df.n_records))
'''

#plt.show()
