import db_connect as db
import pandas as pd
import numpy as np

con = db.engine.connect()

period = 2010
acv_tollerance = 700
receipents = [7800002683, 7800004774, 7800004990]

# update funkcji do  zmiany nazwy po Nowym roku


SQL_statement = '''
select unique ac_nshopid from vldsys_dk.storemonitor
where nc_periodid= {period}
and ac_dtgroup= 'VOLUMETRIC'
and ac_monitortag= '999999' and ac_monitorstatus = 3
'''
SQL_statement = SQL_statement.format(period=period)

outpt2 = con.execute(SQL_statement)
df2 = pd.DataFrame(outpt2.fetchall())
df2.columns = outpt2.keys()
df2.ac_nshopid = df2.ac_nshopid.astype(float)

df2['ac_nshopid'] = df2['ac_nshopid'] - 7800000000

final = []
for receipent in receipents:
    file = r'X:\sample_DK.xlsx'
    data = pd.ExcelFile(file)
    print(data.sheet_names)
    df1 = data.parse('sample_dk')
    print(df1.info())
    donors = pd.merge(
        df1[['SHOP', 'ActiveInSample', 'cel_id', 'ACV', 'DK_SHOPSIZE', 'DK_RETAILER', 'DK_AREA']], df2,
        left_on='SHOP', right_on='ac_nshopid', how='right')
    donors.ac_nshopid = donors.ac_nshopid.astype(int)
    donors.ac_nshopid = donors.ac_nshopid.astype(str)
    donors = donors[donors['ActiveInSample'] == 'YES']
    donors.ACV = donors.ACV.astype(int)
    #str(receipent - 1800000000)
    receipent_char = df1[df1['SHOP'] == receipent - 7800000000].reset_index()
    print(receipent_char)

    donors['ACV_text'] = donors.apply(lambda row: abs(row['ACV'] - receipent_char.at[0, 'ACV']) < acv_tollerance, axis=1)
    filtr = (donors['cel_id'] == receipent_char.at[0, 'cel_id']) & (donors['DK_AREA'] == receipent_char.at[0, 'DK_AREA'])

    donors = donors[filtr][donors['ACV_text']]
    empty = pd.Series([None, None, None, None, None], index=['SHOP', 'ActiveInSample', 'ACV', 'cel_id', 'DK_AREA'])

    result = pd.concat([receipent_char[['SHOP', 'ActiveInSample', 'ACV', 'cel_id', 'DK_AREA']],
                        donors[['SHOP', 'ActiveInSample', 'ACV', 'cel_id', 'DK_AREA']],])
    final.append(result)

donory= pd.concat(final)
print(donory)
