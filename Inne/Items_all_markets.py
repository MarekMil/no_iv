import pandas as pd
import numpy as np

import db_connect as db
import db_connect_Valprda as dbV

con = db.engine.connect()

period = 1993


file_name = r'C:\Users\olwo7001\Desktop\SIRVAL_Norway\2018_12_1994\volumetric\BannerChecks_{period}.pdf'.format(period=period)

SQL_statement = '''
select ac_nshopid, f_nan_key
from vldrawdata_no.rawdata
where nc_periodid in (1995) and ac_dtgroup in ('VOLUMETRIC') and nc_slot2>0
'''


print(SQL_statement)

outpt = con.execute(SQL_statement)
df = pd.DataFrame(outpt.fetchall())
df.columns = outpt.keys()
con.close()
#print(df.head())

con2 = dbV.engine.connect()
SQL_statement2 = '''
SELECT a.msr_mas_id, c.sho_external_code
FROM madras_data.TMMS_MS_RESOLUTION a,
madras_data.trsh_shop c
WHERE msr_tpr_id IN (1006)
AND msr_mas_id IN (1124584,
1124581,
1124583,
1124580,
1124579,
1230686,
1124582,
1124587,
1124588,
1124586,
1124590,
1124596)
AND a.msr_sho_id=c.sho_id
'''


outpt2 = con2.execute(SQL_statement2)
df2 = pd.DataFrame(outpt2.fetchall())
df2.columns = outpt2.keys()
#print(df2.head())
con2.close()

market_df = pd.merge(df, df2,  how='inner', left_on=['ac_nshopid'], right_on=['SHO_EXTERNAL_CODE'])
market_df = market_df[['MSR_MAS_ID', 'f_nan_key']]
market_df = market_df.drop_duplicates()
print(market_df.head())


final = df.groupby(["f_nan_key"]).aggregate({'MSR_MAS_ID':'count'})

final.to_csv(r"C:\Users\olwo7001\Desktop\SIRVAL_Norway\items_per_market.csv", sep=';', index=False)


