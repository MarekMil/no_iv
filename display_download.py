import os, ftplib, glob, zipfile, shutil, socket, time, datetime
from sklepy import mail
from sklepy import dniMiesiace

ftp_user = 'Felt'
ftp_password = 'EYsxOd8D'

ftp_user2 = 'sirvalno'
ftp_password2 = 'June@2016'

def Display( period_change=0):
    try:
        ftp = ftplib.FTP("ftp.acnielsen.no", ftp_user, ftp_password)
        ftp.set_pasv(True)

        dateDct = dniMiesiace()
        year, week = dateDct['year'], dateDct['week']

        #np = r'C:\Users\mima8005\Desktop\Test\W'
        np = r'\\Acn047oslfsv04\s\HHT\W'+str(year[-2:]+str(week))
        np2 = np+'FTP\\'
        np3 = np+'ECL\\'
        delete = r'\\Acn047oslfsv05\T\CAUSAL\Week_'+str(week)
        grfe = r'\\lhrnetapp03cifs.enterprisenet.org\rfeprodapp02\InputFiles\NO\NOR_CAU\\'
        grfe_dl = r'\\lhrnetapp03cifs.enterprisenet.org\rfeprodapp02\InputFiles\NO\Sirpair\\'

        if not os.path.exists(np):
            os.makedirs(np)
        if not os.path.exists(np2):
            os.makedirs(np2)
        if not os.path.exists(np3):
            os.makedirs(np3)

        filename = str(week)+'.*'
        filenameDAT=str(week)+'.*.DAT'
        listDAT=[]
        prevWeek = str(int(week)-3)
        if len(prevWeek) == 1:
            prevWeek = '0'+prevWeek
        prevWeekFilename = str(prevWeek)+'.*'
        os.chdir(np2)

        j = 0
        k = 0

        folderyFTP = ['ECL001','ECL003','ECL004','ECL005','ECL006','ECL007','ECL008','ECL009','ECL010',
                      'ECL011','ECL012','ECL013','ECL014','ECL015','ECL016', 'ECL017','ECL018','ECL019',
                      'ECL020','ECL021','ECL022','ECL023','ECL024','ECL025','ECL026','ECL027','ECL028',
                      'ECL030','ECL033','ECL036','ECL038', 'ECL041','ECL058','ECL061','ECL063','ECL072',
                      'ECL079','ECL086','ECL088','ECL101','ECL122','ECL234','ECL380','ECL871','ECL966',
                      'ECL999']

        folderyECL = ['ECL003','ECL004','ECL007','ECL008','ECL009','ECL011','ECL012','ECL018','ECL019',
                      'ECL025','ECL026','ECL027','ECL028','ECL033','ECL036','ECL041','ECL072','ECL079']

        for i in range(len(folderyFTP)):
            ftp.cwd('//'+folderyFTP[i])
            for file in ftp.nlst(filename):
                j+=1
                fhandle = open(file, 'wb')
                ftp.retrbinary('RETR ' + file, fhandle.write)
                fhandle.close()
        print('Pobrałem: '+str(j)+' plików z FTP.')

        for i in range(len(folderyECL)):
            os.chdir(r'\\acn047oslfsv05\t\CAUSAL\eCollection\\'+folderyECL[i])
            for file in glob.glob(filename):
                k+=1
                shutil.copy2(r'\\acn047oslfsv05\t\CAUSAL\eCollection\\'+folderyECL[i] +'\\'+ file, np3)
        print('Przekopiowałem: '+str(k)+' plików z ECL.')

        os.chdir(np2)
        for file in glob.glob(filename):
            shutil.copy2(np2+file, np+'\\'+file)
        os.chdir(np3)
        for file in glob.glob(filename):
            shutil.copy2(np3+file, np+'\\'+file)
        print('Przekopiowalem pliki do folderu tygodniowego.')

        os.chdir(np)
        for file in glob.glob(filename):
           shutil.copy2(np+'\\'+file, grfe+file)
        print('Wysłałem pliki do zrobienia sirpairki.')

        for file in glob.glob(filename):
             with zipfile.ZipFile(np +'\\' + str(file), "r") as zip_ref:
                 zip_ref.extractall(np+'\\')

        open('ACN.DAT','a').close()
        for file in glob.glob(filenameDAT):
            listDAT.append(file)
        with open(np+'\\'+'ACN.DAT','w') as outfile:
            for fname in listDAT:
                with open(fname) as infile:
                    outfile.write(infile.read())
        print('Wypakowałem pliki i zrobiłem ACN.dat')

        print('Poczekaj. Czyszczę foldery.')

        for i in range(len(folderyECL)):
            os.chdir(r'T:\CAUSAL\eCollection\\'+folderyECL[i])
            for file in glob.glob(prevWeekFilename):
                os.remove(file)

        for i in range(len(folderyFTP)):
            ftp.cwd('//'+folderyFTP[i])
            for file in ftp.nlst(prevWeekFilename):
                ftp.delete(file)
        ftp.quit()

        os.chdir(delete)
        for file in glob.glob('C0*'):
            os.remove(file)
        print('Czekam na wyprodukowanie sirparki')
        time.sleep(1200)


        try:
            os.chdir(grfe_dl)
            for file in glob.glob('PCANO.SIR.PCANODISPLAY.WK'+str(datetime.datetime.now().year)+'0'+
                                  str(week)+'.ZIP.dat'):
                print(file)
                with zipfile.ZipFile(grfe_dl + str(file), "r") as zip_ref:
                    zip_ref.extractall(grfe_dl)

            ftp2 = ftplib.FTP("10.90.148.112", ftp_user2, ftp_password2)
            ftp2.cwd("/RMS/data/sirprdeu/NO/csrmgr/NOsir")

            for file in glob.glob('PCANODISPLAY.'+str(datetime.datetime.now().year)+'-WK-0'+str(week)+'.*'):
                            ftp2.storlines('STOR %s' % str(file), open(file, 'rb'))
            ftp2.quit()
        except:
            print('Nie mogłem wysłać pliku')
            raise


        try:
            mail('DISPLAY')
        except:
            print('Nie udało się wysłać maila')
            raise
        print('Zrobiłem Display (/^▽^)/')
    except socket.timeout:
        Display()

