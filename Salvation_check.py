import db_connect as db
import pandas as pd
import os
from sklepy import period_update, dniMiesiace

con = db.engine.connect()

country = 'no'
#automatyczny update periodu
period = period_update()

dateDct = dniMiesiace()
year, week = dateDct['year'], dateDct['week']

#file_name = r'C:\Users\olwo7001\Desktop\SIRVAL_Norway\2018_21_2003\volumetric\merged.csv'
file_name = r'G:\Team Drives\NO BAU IV\IV_production\\'+str(year)+'w'+str(week)+'\merged.csv'
dir_name = os.path.dirname(file_name)



merged = pd.read_csv(file_name, sep=';', dtype=str, encoding='utf8')
print(merged.info())
stores = ''

for i, x in enumerate(merged.ac_nshopid):
    if i == len(merged.ac_nshopid)-1:
        stores = stores + "'" + x + "'"
    else:
        stores = stores + "'" + x + "',"


print(stores)

SQL_statement3 = '''
select ac_nshopid, AC_MONITORSTATUS
from vldprocess_{country}.storemonitor
where nc_periodid in ('{period}') and ac_dtgroup in ('VOLUMETRIC') and ac_monitortag  in ('999999') and ac_nshopid in ({stores})
'''
SQL_statement3 = SQL_statement3.format(period=period, stores=stores, country=country)

#SQL_statement = SQL_statement.format(country=country_id, shops=shops)

outpt3 = con.execute(SQL_statement3)
df3 = pd.DataFrame(outpt3.fetchall())
df3.columns = outpt3.keys()

periods = ''

for i in range(0, 12):
    if i == 11:
        periods = periods + str(period - 11 + i)
    else:
        periods = periods + str(period - 11 + i) + ", "

SQL_statement = '''
select * from (SELECT s.nc_periodid,S.AC_NSHOPID as stores, t.ac_retailer as retailer, t.AC_EBD AS EBD  
,T.NC_PERIODID AS RTLNR  
,S.AC_MONITORSTATUS AS STATUS    
FROM vldprocess_{country}.STOREMONITOR  S    
JOIN vldinstr_{country}.STORES T ON  S.AC_NSHOPID=T.AC_NSHOPID     
JOIN vldsys_{country}.PERIODS  P ON  S.NC_PERIODID=P.NC_PERIODID  
WHERE S.AC_DTGROUP = 'VOLUMETRIC'  
AND S.AC_MONITORTAG = '999999'   
AND S.NC_PERIODID in (
{periods}
)   
AND T.AC_COUNTRYID = 'NO'   
AND T.AC_CHANNELID = 'SCAN'  
AND AC_SHOPSTATUS in ('REQUIRED')) pivot  
(sum(STATUS) for nc_periodid in (
{periods}
))
'''
SQL_statement = SQL_statement.format(periods=periods, country=country)

#SQL_statement = SQL_statement.format(country=country_id, shops=shops)

outpt = con.execute(SQL_statement)
df = pd.DataFrame(outpt.fetchall())
df.columns = outpt.keys()

con.close()
final = pd.merge(merged, df3, left_on='ac_nshopid', right_on='ac_nshopid', how='left')

print(final)
name = dir_name + '\salvation_check.csv'
name2 = dir_name + r'\Final Store Status_Wk' + str(week) + '.xlsx'

final.to_csv(name, sep=';', index=False)

df.to_excel(name2, 'Sheet1', index=False)

