from sqlalchemy import create_engine
import cx_Oracle

host = 'lhrrhesirp03'
port = '1521'
service_name = 'SIRPRDEU'
user = 'SIRVALREAD_NO'
password = 'SIRVALREAD_NO'
tns = cx_Oracle.makedsn(host, port, service_name=service_name)
cstr = 'oracle://{user}:{password}@{tns}'.format(
    user=user,
    password=password,
    tns=tns
)
print(tns)
engine = create_engine(
    cstr,
    convert_unicode=False,
    pool_recycle=10,
    pool_size=50,
    echo=True,
    encoding = 'utf8'
)
