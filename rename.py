import datetime
import db_connect as db
import sklepy
import display_download
import random
import re
import time

sklep = ''



while sklep != 'q':
    los = random.randrange(1,100,1)
    sklep = str(input(
'''\n  Wyberz akcję:
    • Polecenia do poszczególnych retailerów: mx, hy, co, ly, re, vb, ki, me, ng, st, rs
    • 'daily' - by zrobić CO z konretnego dnia
    • 'midweek' - by zrobić midweeki z CO
    • 'co' - by zrobić CO w poniedziałek
    • 'display'- by zrobić display
    • 'po' - by zrobić MX i HY
    • 'wt' - by zrobić wszystko ze wtorku
    • 'norgrup'- by zrobić VB, KI, ME i NG
    • 'sort' - by posortować plik LY.RA1
    • 'vatRS' - przeliczenie vatu w pliku RS.RA1
    • 'vatST' - przeliczenie vatu w pliku ST.RA1
    • 'info' - aby nadpisac bazowe dni pobierania
    • 'schedule' - zeby zobaczyc orientacyjne godziny przychodzenia danych
    • 'q' - aby zakończyć.\n'''))

    sklep=sklep.lower()
    #znajduje int w kodzie
    if sklep == 'schedule':
        print(
'''     • COOP ~ 14:20
     Poniedzialek:
        • Display ~ 10:10
        • MX ~ 10:20
        • HY ~ 11:05
     Wtorek:
        • LY ~ 6:10
        • RE ~ 8:05
        • ST ~ 8:40
        • VB, KI, ME, NG ~ 9:00
        • RS ~ 9:10
        ''')
        time.sleep(30)

    if sklep == 'info':
        print(
'''Żeby nadpisać ustawienia bazowe, do poleceń sklepów trzeba dodać ile
dni od daty obecnej powinn być robione te dane, np. w czwartek chcemy zrobić 
dane LY, które normalnie przychodzą we wtorek, więc wpisujemy: ly 2.
W przypadku gdy chcemy zrobić dane np. sprzed tygodnia to konieczne jest też 
podanie periodu, po liczbie dni, np. jest czwartek, period: 2024 i chcemy zrobić dane LY 
sprzed tygodnia, wiec wpiszemy: ly 9 2023. (9 bo do wtorku są 2 dni +7, bo tydzień wcześniej).''')
        time.sleep(30)

    try:
        sklep_lst = sklep.split(' ')
        if len(sklep_lst) == 3:
            sklep, past, period = sklep_lst[0], int(sklep_lst[1]), int(sklep_lst[2])
        elif len(sklep_lst) == 2 and len(sklep_lst[1]) <= 3:
            sklep, past = sklep_lst[0], int(sklep_lst[1])
        elif len(sklep_lst) == 2 and len(sklep_lst[1]) == 4:
            sklep, period = sklep_lst[0], int(sklep_lst[1])
    except:
        pass

    if sklep == 'mx' or sklep == 'po':
        try:
            sklepy.MX(past,period)
        except:
            try:
                sklepy.MX(past)
            except:
                sklepy.MX()

    if sklep == 'hy' or sklep == 'po':
        try:
            sklepy.HY(past,period)
        except:
            try:
                sklepy.HY(past)
            except:
                sklepy.HY()

    if sklep == 'co':
        try:
            sklepy.CO(past, period)
        except:
            try:
                sklepy.CO(past)
            except:
                sklepy.CO()

    if sklep == 'daily':
        try:
            sklepy.dailyCO(past,period)
        except:
            try:
                sklepy.dailyCO(past)
            except:
                sklepy.dailyCO()

    if sklep == 'midweek':
        try:
            sklepy.midweekCO(past)
        except:
            sklepy.midweekCO()

    if sklep == 'ly' or sklep == 'wt':
        try:
            sklepy.LY(period)
        except:
            sklepy.LY()

    if sklep == 're' or sklep == 'wt':
        try:
            sklepy.RE(past,period)
        except:
            try:
                sklepy.RE(past)
            except:
                sklepy.RE()

    if sklep == 'vb' or sklep == 'wt' or sklep == 'norgrup':
        try:
            sklepy.VB(past, period)
        except:
            try:
                sklepy.VB(past)
            except:
                sklepy.VB()

    if sklep == 'ki' or sklep == 'wt' or sklep == 'norgrup':
        try:
            sklepy.KI(past, period)
        except:
            try:
                sklepy.KI(past)
            except:
                sklepy.KI()

    if sklep == 'me' or sklep == 'wt' or sklep == 'norgrup':
        try:
            sklepy.ME(past,period)
        except:
            try:
                sklepy.ME(past)
            except:
                sklepy.ME()

    if sklep == 'ng' or sklep == 'wt' or sklep == 'norgrup':
        try:
            sklepy.NG(past, period)
        except:
            try:
                sklepy.NG(past)
            except:
                sklepy.NG()

    if sklep == 'st' or sklep == 'wt':
        import ST_download
        try:
            ST_download.ST(past,period)
        except:
            try:
                ST_download.ST(past)
            except:
                ST_download.ST()

    if sklep == 'rs' or sklep == 'wt':
        try:
            sklepy.RS(past, period)
        except:
            try:
                sklepy.RS(past)
            except:
                sklepy.RS()

    if sklep =='display':
        try:
            display_download.Display(period)
        except:
            display_download.Display()

    if sklep =='vatrs':
        import RS
        try:
            RS.vatRS(period)
        except:
            RS.vatRS()

    if sklep =='vatst':
        import statoil
        try:
            statoil.vatST(period)
        except:
            statoil.vatST()

    if sklep == 'sort':
        import sort_file
        try:
            sort_file.sort(period)
        except:
            sort_file.sort()


    if los ==1:
        print('''      
         ▄▄▄▀▀▀▄▄███▄
          ▄▀▀░░░░░░░▐░▀██▌
     ▄▀░░░░▄▄███░▌▀▀░▀█
  ▄█░░▄▀▀▒▒▒▒▒▄▐░░░░█▌
▐█▀▄▀▄▄▄▄▀▀▀▀▌░░░░░▐█▄
▌▄▄▀▀                  ▌░░░░▄███████▄
                           ▐░░░░▐███████████▄
                           ▐░░░░▐█████████████▄
                             ▀▄░░░▐██████████████▄
                                 ▀▄▄████████████████▄
                                                   █▀██████▀▀▀▀█▄
                                             ▄▄▀▄▀  ▀██▀▀▀▄▄▄▀█
                                      ▄▀▀▀▀▀         ██▌
                                                        ▄▀▄▀
                                                  ▄▄██▀''')
        print('Nawiedził Ciebie tukan pomyślności. Od tej pory nie zaliczysz już nigdy fuckupu.')
