from sqlalchemy import create_engine
import cx_Oracle

host = 'oravalprdscan.enterprisenet.org'
port = '1535'
service_name = 'VALPRDMDR'
user = 'NRSP_GUEST'
password = 'NRSP_GUEST'
tns = cx_Oracle.makedsn(host, port, service_name=service_name)
print(tns)
cstr = 'oracle://{user}:{password}@{tns}'.format(
    user=user,
    password=password,
    tns=tns
)

engine = create_engine(
    cstr,
    convert_unicode=False,
    pool_recycle=10,
    pool_size=50,
    echo=True
)
